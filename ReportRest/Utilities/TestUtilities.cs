﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http.Results;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;

namespace ReportRest.Utilities
{
    public abstract class TestUtilities : DocumentsService
    {

        protected const int InitialLength = 100;
        protected const byte InitialValue = 0x20;
        protected const byte SecondInitialValue = 0x30;

        protected T ConvertResponseToObject<T>(ResponseMessageResult response)
        {
            var result = response.Response.Content.ReadAsAsync<T>();
            return (T)Convert.ChangeType(result.Result, typeof(T));
        }

        protected MyStatus GetFakeMyStatusWithoutImageInDevices()
        {
            return new MyStatus
            {
                SingleStatus = GetFakeStatus("1", 1)
            };
        }

        protected List<Employee> GetFakeEmployees(int constructionSiteId, string userId)
        {
            return new List<Employee> { GetFakeEmployee(constructionSiteId, userId) };
        }

        protected PendingEvent GetFakePendingEvent(string inviterId)
        {
            return new PendingEvent
            {
                Hash = "Blablabla",
                InviterId = inviterId,
                EventType = PendingEvent.Event.Registration,
                ConstructionSiteId = 111222,
                IsActive = true,
                CreatedAt = new DateTime(1996, 07, 05)
            };
        }

        protected MyConstructionSite GetFakeMyConstructionSiteWithoutImageInDevice()
        {
            MyConstructionSite myConstructionSite = GetFakeMyConstructionSite();
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                foreach (MyStatus myMultipleStatus in myDefect.MyMultipleStatus)
                {
                    myMultipleStatus.ImagesInDevice = null;
                }
            }
            return myConstructionSite;
        }

        protected MyConstructionSite GetFakeMyConstructionSite()
        {
            return new MyConstructionSite()
            {
                SingleConstructionSite = GetFakeConstructionSite("1"),
                SingleLocation = GetFakeLocation("1"),
                Employees = GetFakeEmployees(1, "1"),
                MyDefects = new List<MyDefect> { GetFakeMyDefect() }
            };
        }

        protected MyDefect GetFakeMyDefect()
        {
            return new MyDefect()
            {
                SingleDefect = GetFakeDefect("1", 1),
                MyMultipleStatus = new List<MyStatus> { GetFakeMyStatus() }
            };
        }

        protected MyStatus GetFakeMyStatus()
        {
            return new MyStatus()
            {
                SingleStatus = GetFakeStatus("1", 1),
                ImagesInDevice = new List<ImageInDevice> { GetFakeImageInDevice(1, 1) }
            };
        }

        protected int GetIdOfNonExistingDefect()
        {
            for (int i = 0; i < 1000; i++)
            {
                if (_defectsService.GetDefect(i) == null)
                {
                    return i;
                }
            }
            return 0;
        }

        protected int GetIdOfNonExistingConstructionSite()
        {
            for (int i = 0; i < 1000; i++)
            {
                if (_constructionSitesService.GetConstructionSite(i) == null)
                {
                    return i;
                }
            }
            return 0;
        }

        protected ImageInDevice PostFakeImageInDevice(int deviceId, int statusId)
        {
            ImageInDevice imageInDevice = GetFakeImageInDevice(deviceId, statusId);
            _imageInDevicesService.PostImageInDevice(imageInDevice);
            return _imageInDevicesService.GetImageInDevices()
                .FirstOrDefault(o => o.Compare(imageInDevice));
        }

        protected Employee GetFakeEmployee(int constructionSiteId, string userId)
        {
            return new Employee
            {
                UserId = userId,
                ConstructionSiteId = constructionSiteId,
                IsManager = false,
                IsActive = true,
                CreatedAt = new DateTime(1996, 07, 05)
            };
        }

        protected Employee PostFakeEmployee(int constructionSiteId, string userId)
        {
            Employee employee = GetFakeEmployee(constructionSiteId, userId);
            Employee fromDatabase = _employeesService.GetEmployees().FirstOrDefault(
                o => o.Compare(employee));
            if (fromDatabase != null)
            {
                return fromDatabase;
            }
            List<string> errorList = _employeesService.PostEmployee(employee);
            Employee toReturn = _employeesService.GetEmployees().FirstOrDefault(o => o.Compare(employee));
            return toReturn;
        }

        protected ConstructionSite GetFakeConstructionSite(string creatorId)
        {
            return new ConstructionSite()
            {
                CreatorId = creatorId,
                CreatedAt = new DateTime(1996, 07, 05),
                IsActive = true
            };
        }

        protected ImageInDevice GetFakeImageInDevice(int deviceId, int statusId)
        {
            return new ImageInDevice
            {
                DeviceId = deviceId,
                StatusId = statusId, 
                ImageId = 1,
                ImageLocation = "c",
                CreatedAt = new DateTime(1996, 07, 05),
                IsActive = true
            };
        }

        protected List<MyStatus> GetFakeMyStatuses()
        {
            return new List<MyStatus>
            {
                GetFakeMyStatus()
            };
        } 

        protected Status GetFakeStatus(string creatorId, int defectId)
        {
            return new Status
            {
                CreatorId = creatorId,
                ShortDescription = "blabla",
                DefectId = defectId,
                CreatedAt = new DateTime(1996, 07, 05),
                IsActive = true
            };
        }

        protected Location PostFakeLocation(string creatorId)
        {
            Location location = GetFakeLocation(creatorId);
            _locationsService.PostLocation(location);
            Location locationFromDatabase = _locationsService.GetLocations().FirstOrDefault(o => o.Compare(location));
            return locationFromDatabase;
        }

        protected Location GetFakeLocation(string creatorId)
        {
            return new Location
            {
                CreatorId = creatorId,
                ConstructionSiteId = 1,
                Longitude = 1,
                Latitude = 1,
                Address = "Kaunas",
                Description = "Good",
                CreatedAt = new DateTime(1996, 07, 05),
                IsActive = true
            };
        }

        protected Defect GetFakeDefect(string creatorId, int constructionSiteId)
        {
            return new Defect
            {
                CreatorId = creatorId,
                ConstructionSiteId = constructionSiteId,
                IsFixed = false,
                IsActive = true,
                CreatedAt = new DateTime(1996, 07, 05)
            };
        }

        protected List<ImageInDevice> GetFakeImageInDevices(int deviceId)
        {
            return new List<ImageInDevice> {GetFakeImageInDevice(deviceId, 1)};
        }

        protected MyImage GetFakeMyImage(int fakeId, int initialLength, byte initialValue)
        {
            return new MyImage
            {
                SingleImage = GetFakeImage(1, initialLength, initialValue),
                ImageInDevices = new List<ImageInDevice> {GetFakeImageInDevice(fakeId, 1)}
            };
        }

        protected Defect PostFakeDefect(string creatorId, int constructionSiteId)
        {
            Defect defect = GetFakeDefect(creatorId, constructionSiteId);
            Defect fromDatabase = _defectsService.GetDefects().
                FirstOrDefault(o => o.Compare(defect));
            if (fromDatabase != null)
            {
                return fromDatabase;
            }
            _defectsService.PostDefect(defect);
            return _defectsService.GetDefects().FirstOrDefault(o => o.Compare(defect));
        }

        protected ConstructionSite PostFakeConstructionSite(string creatorId)
        {
            ConstructionSite constructionSite = GetFakeConstructionSite(creatorId);
            ConstructionSite fromDatabase = _constructionSitesService.GetConstructionSites()
                .SingleOrDefault(o => o.Compare(constructionSite));
            if (fromDatabase != null)
            {
                return fromDatabase;
            }
            _constructionSitesService.PostConstructionSite(constructionSite);
            ConstructionSite constructionSiteFromDatabase = _constructionSitesService.GetConstructionSites()
                 .FirstOrDefault(o => o.Compare(constructionSite));
            return constructionSiteFromDatabase;
        }

        protected Image GetFakeImage(int statusId, int byteArrayLength, byte byteArrayValue)
        {
            return new Image
            {
                StatusId = statusId,
                CreatedAt = new DateTime(1996, 07, 05),
                PhotoInBytes = CreateByteArrayForTests(byteArrayLength, byteArrayValue),
                IsActive = true
            };
        } 

        protected bool DatabaseSizeCorrect(int initialSize, int sizeAfterCreation, int sizeAfterDeletion)
        {
            return initialSize == sizeAfterDeletion && sizeAfterCreation == initialSize + 1;
        }

        protected byte[] CreateByteArrayForTests(int length, byte initialValue)
        {
            byte[] byteArray = new byte[length];
            for (int i = 0; i < length; i++)
            {
                byteArray[i] = initialValue;
            }
            return byteArray;
        }

        protected bool ThisLengthAndContainsMessage(List<string> errorList, string message, int length)
        {
            return errorList.ToArray().Length == length && errorList.Contains(message);
        }

        protected bool ThisLengthAndContainsInteger(List<int> idsList, int integer, int length)
        {
            return idsList.Contains(integer) && idsList.ToArray().Length == length;
        }

        protected Status PostFakeStatus(string creatorId, int defectId)
        {
            Status status = GetFakeStatus(creatorId, defectId);
            Status fromDatabase = _statusService.GetMultipleStatus().
                FirstOrDefault(o => o.Compare(status));
            if (fromDatabase != null)
            {
                return fromDatabase;
            }
            _statusService.PostStatus(status);
            return _statusService.GetMultipleStatus().FirstOrDefault(o => o.Compare(status));
        }

    }
}