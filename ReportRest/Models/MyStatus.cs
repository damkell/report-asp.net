﻿using ReportRest.Documents;
using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Models
{
    public class MyStatus
    {
        public Status SingleStatus { get; set; }
        public List<ImageInDevice> ImagesInDevice { get; set; }

        public bool Compare(MyStatus otherStatus)
        {
            if (otherStatus == null)
            {
                return false;
            }
            return ((SingleStatus != null && SingleStatus.Compare(otherStatus.SingleStatus)) ||
                    SingleStatus == null && otherStatus.SingleStatus == null) &&
                   CompareImagesInDeviceLists(otherStatus.ImagesInDevice);
        }

        private bool CompareImagesInDeviceLists(List<ImageInDevice> otherImageInDevices)
        {
            if (ImagesInDevice != null && otherImageInDevices != null)
            {
                return otherImageInDevices.Where(e2 => !ImagesInDevice.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0;
            }
            return ImagesInDevice == null && otherImageInDevices == null;
        }

    }
}
