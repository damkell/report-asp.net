﻿using ReportRest.Documents;
using System.Collections.Generic;

namespace ReportRest.Models
{
    class MyUser
    {
        public List<PendingEvent> PendingEvents { get; set; }
        public List<MyConstructionSite> MyConstructionSites { get; set; }
        public string Email { get; set; }
        public string UserName { get; set; }
        public bool EmailConfirmed { get; set; }
    }
}
