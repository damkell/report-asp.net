﻿using ReportRest.Documents;
using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Models
{
    public class MyImage
    {
        public Image SingleImage { get; set; }
        public List<ImageInDevice> ImageInDevices { get; set; }

        public bool Compare(MyImage other)
        {
            if (other == null)
            {
                return false;
            }
            return ((SingleImage != null && SingleImage.Compare(other.SingleImage)) ||
                    SingleImage == null && other.SingleImage == null) &&
                   CompareImageInDevicesLists(other.ImageInDevices);
        }

        private bool CompareImageInDevicesLists(List<ImageInDevice> otherImageInDevices)
        {
            if (ImageInDevices != null && otherImageInDevices != null)
            {
                return otherImageInDevices.Where(e2 => !ImageInDevices.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0;
            }
            return ImageInDevices == null && otherImageInDevices == null;
        }

    }
}
