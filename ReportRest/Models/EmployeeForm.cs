﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ReportRest.Models
{
    public class EmployeeForm
    {
        private const string EmailNotSpecified = "email.not.specified";
        private const string ConstructionSiteIdNotSpecified = "construction.site.id.not.specified";

        public int ConstructionSiteId { get; set; }
        public string Email { get; set; }
        public bool IsManager { get; set; }

        public List<string> ValidationErrors()
        {
            List<string> errorList = new List<string>();
            if (ConstructionSiteId == 0)
            {
                errorList.Add(ConstructionSiteIdNotSpecified);
            }
            if (string.IsNullOrEmpty(Email))
            {
                errorList.Add(EmailNotSpecified);
            }
            if (errorList.Count != 0)
            {
                return errorList;
            }
            return null;
        } 

    }
}