﻿using ReportRest.Documents;
using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Models
{
    public class MyConstructionSite
    {
        public ConstructionSite SingleConstructionSite { get; set; }
        public Location SingleLocation { get; set; }
        public List<MyDefect> MyDefects { get; set; }
        public List<Employee> Employees { get; set; } 

        public bool Compare(MyConstructionSite otherSite)
        {
            if (otherSite == null)
            {
                return false;
            }
            return ((SingleConstructionSite != null && SingleConstructionSite.Compare(otherSite.SingleConstructionSite)) ||
                    (SingleConstructionSite == null && otherSite.SingleConstructionSite == null)) &&
                   ((SingleLocation != null && SingleLocation.Compare(otherSite.SingleLocation)) ||
                    (SingleLocation == null && otherSite.SingleLocation == null)) &&
                   CompareMyDefectsLists(otherSite.MyDefects) &&
                   CompareEmployeesLists(otherSite.Employees);
        }

        private bool CompareMyDefectsLists(List<MyDefect> otherList)
        {
            if (MyDefects != null && otherList != null)
            {
                return otherList.Where(e2 => !MyDefects.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0;
            }
            return MyDefects == null && otherList == null;
        }

        private bool CompareEmployeesLists(List<Employee> otherList)
        {
            if (Employees != null && otherList != null)
            {
                return otherList.Where(e2 => !Employees.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0;
            }
            return Employees == null && otherList == null;
        }

    }
}
