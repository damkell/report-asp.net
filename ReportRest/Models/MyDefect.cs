﻿using ReportRest.Documents;
using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Models
{
    public class MyDefect
    {
        public Defect SingleDefect { get; set; }
        public List<MyStatus> MyMultipleStatus { get; set; }

        public bool Compare(MyDefect otherDefect)
        {
            if (otherDefect == null)
            {
                return false;
            }
            return (SingleDefect != null && SingleDefect.Compare(otherDefect.SingleDefect)
                || (SingleDefect == null && otherDefect.SingleDefect == null)) &&
                CompareMyStatusLists(otherDefect.MyMultipleStatus);
        }

        private bool CompareMyStatusLists(List<MyStatus> otherMultipleStatus)
        {
            if (MyMultipleStatus != null && otherMultipleStatus != null)
            {
                return otherMultipleStatus.Where(e2 => !MyMultipleStatus.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0;
            }
            return MyMultipleStatus == null && otherMultipleStatus == null;
        }
    }
}
