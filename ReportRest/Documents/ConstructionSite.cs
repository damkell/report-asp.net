﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Documents
{
    public class ConstructionSite
    {
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }

        public bool Compare(ConstructionSite otherSite)
        {
            if (CreatorId != otherSite?.CreatorId)
            {
                return false;
            }
            if (CreatedAt.CompareTo(otherSite.CreatedAt) != 0)
            {
                return false;
            }
            return IsActive == otherSite.IsActive;
        }

    }

}
