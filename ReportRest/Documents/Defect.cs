﻿using System;

namespace ReportRest.Documents
{
    public class Defect
    {       
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public int ConstructionSiteId { get; set; }
        public bool IsFixed { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedAt { get; set; }

        public bool Compare(Defect other)
        {
            if (CreatorId != other?.CreatorId)
            {
                return false;
            }
            if (ConstructionSiteId != other.ConstructionSiteId)
            {
                return false;
            }
            if (IsActive != other.IsActive)
            {
                return false;
            }
            return CreatedAt.CompareTo(other.CreatedAt) == 0;
        }

    }

}
