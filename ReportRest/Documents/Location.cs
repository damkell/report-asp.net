﻿using System;

namespace ReportRest.Documents
{
    public class Location
    {
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public int ConstructionSiteId { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string Address { get; set; }
        public string Description { get; set; }
        public DateTime CreatedAt {get; set; }
        public bool IsActive { get; set; }

        public bool Compare(Location otherLocation)
        {
            if (ConstructionSiteId != otherLocation?.ConstructionSiteId)
            {
                return false;
            }
            if (CreatorId != otherLocation.CreatorId)
            {
                return false;
            }
            if (Longitude != otherLocation.Longitude)
            {
                return false;
            }
            if (Latitude != otherLocation.Latitude)
            {
                return false;
            }
            if (Address != otherLocation.Address)
            {
                return false;
            }
            if (Description != otherLocation.Description)
            {
                return false;
            }
            if (CreatedAt.CompareTo(otherLocation.CreatedAt) != 0)
            {
                return false;
            }
            return IsActive == otherLocation.IsActive;
        }

    }
}
