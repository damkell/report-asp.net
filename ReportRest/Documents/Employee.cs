﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ReportRest.Documents
{
    public class Employee
    {
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public int ConstructionSiteId { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }
        public string UserId { get; set; }
        public bool IsManager { get; set; }

        public bool Compare(Employee other)
        {
            if (UserId != other.UserId)
            {
                return false;
            }
            if (ConstructionSiteId != other.ConstructionSiteId)
            {
                return false;
            }
            if (CreatorId != other.CreatorId)
            {
                return false;
            }
            if (UserId != other.UserId)
            {
                return false;
            }
            if (CreatedAt.CompareTo(other.CreatedAt) != 0)
            {
                return false;
            }
            if (IsActive != other.IsActive)
            {
                return false;
            }
            return true;
        }

    }
}