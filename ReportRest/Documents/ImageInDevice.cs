﻿using System;

namespace ReportRest.Documents
{
    public class ImageInDevice
    {
        public int Id { get; set; }
        public int DeviceId { get; set; }
        public int StatusId { get; set; }
        public int ImageId { get; set; }
        public string ImageLocation { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }

        public bool Compare(ImageInDevice other)
        {
            if (DeviceId != other?.DeviceId)
            {
                return false;
            }
            if (StatusId != other.StatusId)
            {
                return false;
            }
            if (ImageId != other.ImageId)
            {
                return false;
            }
            if (ImageLocation != other.ImageLocation)
            {
                return false;
            }
            if (CreatedAt.CompareTo(other.CreatedAt) != 0)
            {
                return false;
            }
            return IsActive == other.IsActive;
        }

    }
}
