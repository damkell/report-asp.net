﻿using System;

namespace ReportRest.Documents
{
    public class PendingEvent
    {

        public enum Event {Invitation, Registration, Addition};

        public int Id { get; set; }
        public Event EventType { get; set; }
        public string Email { get; set; }
        public string UserId { get; set; }
        public string Hash { get; set; }
        public string InviterId { get; set; }
        public int ConstructionSiteId { get; set; }
        public bool IsActive { get; set; }
        public bool IsManager { get; set; }
        public DateTime CreatedAt { get; set; }

        public bool Compare(PendingEvent otherPendingEvent)
        {
            if (Email != otherPendingEvent.Email)
            {
                return false;
            }
            if (EventType != otherPendingEvent.EventType)
            {
                return false;
            }
            if (InviterId != otherPendingEvent.InviterId)
            {
                return false;
            }
            if (ConstructionSiteId != otherPendingEvent.ConstructionSiteId)
            {
                return false;
            }
            if (IsManager != otherPendingEvent.IsManager)
            {
                return false;
            }
            return IsActive == otherPendingEvent.IsActive;
        }

    }

}
