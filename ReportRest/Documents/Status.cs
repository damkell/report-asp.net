﻿using System;

namespace ReportRest.Documents
{
    public class Status
    {
        public int Id { get; set; }
        public string CreatorId { get; set; }
        public string ShortDescription { get; set; }
        public int DefectId { get; set; }
        public DateTime CreatedAt { get; set; }
        public bool IsActive { get; set; }

        public bool Compare(Status otherStatus)
        {
            if (CreatorId != otherStatus?.CreatorId)
            {
                return false;
            }
            if (ShortDescription != otherStatus.ShortDescription)
            {
                return false;
            }
            if (DefectId != otherStatus.DefectId)
            {
                return false;
            }
            return true;
        }

    }
}
