﻿using ReportRest.Services;
using System;

namespace ReportRest.Documents
{
    public class Image
    {
        public int Id { get; set; }
        public int StatusId { get; set; }
        public DateTime CreatedAt { get; set; }
        public byte[] PhotoInBytes { get; set; }
        public bool IsActive { get; set; }

        public bool Compare(Image otherImage)
        {
            if (StatusId != otherImage?.StatusId)
            {
                return false;
            }
            if (CreatedAt.CompareTo(otherImage.CreatedAt) != 0)
            {
                return false;
            }
            return IsActive == otherImage.IsActive && ImageCompareService.ByteArrayCompare(PhotoInBytes, otherImage.PhotoInBytes);
        }
       
    }
}
