﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ImageInDeviceTest : TestUtilities
    {
        [Test]
        public void TestIfCompareMethodReturnsTrueInSame()
        {
            ImageInDevice imageInDevice = GetFakeImageInDevice(1, 1);
            Assert.IsTrue(imageInDevice.Compare(imageInDevice));
        }

        [Test]
        public void TestIfCompareMethodReturnsFalseInDifferent()
        {
            ImageInDevice imageInDevice = GetFakeImageInDevice(1,1);
            ImageInDevice otherImageInDevice = GetFakeImageInDevice(2,2);
            Assert.IsFalse(imageInDevice.Compare(otherImageInDevice));
        }

    }
}
