﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using ReportRest.Utilities;

namespace ReportRest.Tests
{

    [TestFixture]
    public class EmployeesServiceTest : TestUtilities
    {

        [Test]
        public void TestIfServiceValidatesEmployee()
        {
            Employee employee = GetFakeEmployee(0, "");
            int initialDatabaseSize = GetCurrentDatabaseSize();
            List<string> validationErrors = _employeesService.PostEmployee(employee);
            int sizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(initialDatabaseSize == sizeAfterAttempt &&
                          validationErrors.Count == 2 &&
                          validationErrors.Contains(EmployeesService.ConstructionSiteIdNotSpecified) &&
                          validationErrors.Contains(EmployeesService.UserIdNotSpecified));
        }

        [Test]
        public void TestIfServicePostsAndDeletesEmployee()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Employee employee = PostFakeEmployee(1, "1");
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _employeesService.DeleteEmployee(employee.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(initialDatabaseSize,databaseSizeAfterCreation,
                databaseSizeAfterDeletion));
        }

        private int GetCurrentDatabaseSize()
        {
            return _employeesService.GetEmployees().ToList().Count;
        }


    }
}