﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using NUnit.Framework;
using ReportRest.Controllers;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using ReportRest.Utilities;
using ReportRest.Validations;

namespace ReportRest.Tests
{
    [TestFixture]
    public class MyConstructionSitesControllerTest : TestUtilities
    {

        [Test]
        public void TestIfLocationInheritsConstructionSiteIdInPost()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            Location location = GetFakeLocation("1");
            ConstructionSite constructionSite = GetFakeConstructionSite("1");
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                SingleLocation = location
            };
            controller.AddConstructionSite(myConstructionSite);
            Location locationFromDatabase = _locationsService.GetLocations().FirstOrDefault(
                o => o.Compare(location));
            ConstructionSite constructionSiteFromDatabase = _constructionSitesService.GetConstructionSites().
                FirstOrDefault(o => o.Compare(constructionSite));
            Assert.NotNull(locationFromDatabase);
            Assert.NotNull(constructionSiteFromDatabase);
            Employee employeeFromDatabase = _employeesService.GetEmployees()
                .Single(o => o.ConstructionSiteId == constructionSiteFromDatabase.Id);
            Assert.NotNull(employeeFromDatabase);
            Assert.AreEqual(locationFromDatabase.ConstructionSiteId, constructionSiteFromDatabase.Id);
            _constructionSitesService.DeleteConstructionSite(constructionSiteFromDatabase.Id);
            _locationsService.DeleteLocation(locationFromDatabase.Id);
            _employeesService.DeleteEmployee(employeeFromDatabase.Id);
        }

        [Test]
        public void TestIfControllerCreatesNewEmployeeOnFirstConstructionSiteUpload()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            string creatorId = "1";
            ConstructionSite constructionSite = GetFakeConstructionSite(creatorId);
            Location location = GetFakeLocation(creatorId);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                SingleLocation = location
            };
            ResponseMessageResult message = 
                controller.AddConstructionSite(myConstructionSite);
            ConstructionSite siteFromDatabase = _constructionSitesService.GetConstructionSites().
                SingleOrDefault(o => o.Compare(constructionSite));
            Assert.NotNull(siteFromDatabase);
            Employee employeeFromDatabase = _employeesService.GetEmployees().
                SingleOrDefault(o => o.ConstructionSiteId == siteFromDatabase.Id);
            Assert.NotNull(employeeFromDatabase);
            Location locationFromDatabase = _locationsService.GetLocations().
                SingleOrDefault(o => o.ConstructionSiteId == siteFromDatabase.Id);
            Assert.NotNull(locationFromDatabase);
            _employeesService.DeleteEmployee(employeeFromDatabase.Id);
            _locationsService.DeleteLocation(locationFromDatabase.Id);
            _constructionSitesService.DeleteConstructionSite(siteFromDatabase.Id);
        }

        [Test]
        public void TestIfControllerAddMethodReturnsErrorWithoutMandatoryFields()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite site = new MyConstructionSite();
            List<string> errorList = ConvertResponseToObject<List<string>>(
                controller.AddConstructionSite(site));
            Assert.IsTrue(errorList.Count == 2 &&
                          errorList.Contains(MyConstructionSiteValidator.ConstructionSiteNotSpecified) &&
                          errorList.Contains(MyConstructionSiteValidator.LocationNotSpecified));
        }

        [Test]
        public void TestIfControllerUpdateMethodReturnsErrorWithoutMandatoryFields()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite site = new MyConstructionSite();
            List<string> errorList = ConvertResponseToObject<List<string>>(
                controller.UpdateConstructionSite(site));
            Assert.IsTrue(errorList.Count == 4 &&
                          errorList.Contains(MyConstructionSiteValidator.ConstructionSiteNotSpecified) &&
                          errorList.Contains(MyConstructionSiteValidator.LocationNotSpecified) &&
                          errorList.Contains(MyConstructionSiteValidator.DefectsNotSpecified) &&
                          errorList.Contains(MyConstructionSiteValidator.EmployeesNotSpecified));
        }

        [Test]
        public void TestIfControllerUploadMethodValidatesSingleConstructionSite()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = GetFakeConstructionSite("");
            MyConstructionSite myConstructionSite = GetFakeMyConstructionSite();
            myConstructionSite.SingleConstructionSite = constructionSite;
            ObjectValidationError<ConstructionSite> expectedError = new ObjectValidationError<ConstructionSite>
            {
                Object = constructionSite,
                ErrorList = new List<string> { ConstructionSitesService.CreatorIdNotSpecified}
            };
            ObjectValidationError<ConstructionSite> actualError =
                ConvertResponseToObject<ObjectValidationError<ConstructionSite>>(
                    controller.AddConstructionSite(myConstructionSite));
            Assert.IsTrue(expectedError.Compare(actualError));
        }

        [Test]
        [Category("problematic")]
        public void TestIfControllerDoesCrudWhenDefectsDifferWithoutImageInDevice()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Employee employee = PostFakeEmployee(constructionSite.Id, "1");
            Defect firstDefect = PostFakeDefect("1", constructionSite.Id);
            Defect secondDefect = GetFakeDefect("2", constructionSite.Id);
            secondDefect.CreatedAt = new DateTime(1997,07,05);
            Status firstStatus = GetFakeStatus("1", firstDefect.Id);
            Status secondStatus = GetFakeStatus("2", 0);
            secondStatus.CreatedAt = new DateTime(1997, 07, 05);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                Employees = new List<Employee> {employee},
                SingleLocation = GetFakeLocation("1"),
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = new List<MyStatus> {new MyStatus { SingleStatus = firstStatus}}
                    },
                    new MyDefect
                    {
                        SingleDefect = secondDefect,
                        MyMultipleStatus = new List<MyStatus> {new MyStatus {SingleStatus = secondStatus} }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Defect secondDefectFromDatabase = _defectsService.GetDefects().
                FirstOrDefault(o => o.Compare(secondDefect));
            Assert.NotNull(secondDefectFromDatabase);
            secondDefectFromDatabase.IsFixed = true;
            Status secondStatusFromDatabase = _statusService.GetMultipleStatus().
                FirstOrDefault(o => o.Compare(secondStatus));
            myConstructionSite.MyDefects = new List<MyDefect>
            {
                new MyDefect
                {
                    MyMultipleStatus = new List<MyStatus>
                    { new MyStatus { SingleStatus = secondStatusFromDatabase} },
                    SingleDefect = secondDefectFromDatabase
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Defect defectShouldBeDeleted = _defectsService.GetDefect(firstDefect.Id);
            Status statusShouldBeDeteled = _statusService.GetMultipleStatus().
                SingleOrDefault(o => o.Compare(firstStatus));
            Assert.NotNull(defectShouldBeDeleted);
            Assert.IsFalse(defectShouldBeDeleted.IsActive);
            Assert.NotNull(statusShouldBeDeteled);
            Assert.IsFalse(statusShouldBeDeteled.IsActive);
            Defect updatedDefect = _defectsService.GetDefect(secondDefectFromDatabase.Id);
            Assert.NotNull(updatedDefect);
            Assert.IsTrue(updatedDefect.IsFixed);
            _defectsService.DeleteDefect(defectShouldBeDeleted.Id);
            _statusService.DeleteStatus(statusShouldBeDeteled.Id);
            _defectsService.DeleteDefect(updatedDefect.Id);
            _statusService.DeleteStatus(secondStatus.Id);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _employeesService.DeleteEmployee(employee.Id);
        }

        [Test]
        public void TestIfControllerDoesCrudWhenDefectsDifferWithImageInDevice()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Employee employee = PostFakeEmployee(constructionSite.Id, "1");
            Defect firstDefect = GetFakeDefect(constructionSite.CreatorId, 
                constructionSite.Id);
            Defect secondDefect = GetFakeDefect(constructionSite.CreatorId + 1,
                constructionSite.Id);
            Status firstStatus = GetFakeStatus(firstDefect.CreatorId,
                firstDefect.Id);
            Status secondStatus = GetFakeStatus(secondDefect.CreatorId,
                secondDefect.Id);
            ImageInDevice firstImageInDevice = GetFakeImageInDevice(1, firstStatus.Id);
            ImageInDevice secondImageInDevice = GetFakeImageInDevice(2, secondStatus.Id);
            MyConstructionSite myConstructionSite = new MyConstructionSite()
            {
                Employees = new List<Employee> {employee},
                SingleLocation = GetFakeLocation("1"),
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = firstStatus,
                                ImagesInDevice = new List<ImageInDevice> {firstImageInDevice}
                            }
                        }
                    },
                    new MyDefect
                    {
                        SingleDefect = secondDefect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = secondStatus,
                                ImagesInDevice = new List<ImageInDevice> {secondImageInDevice}
                            }
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Defect firstDefectFromDatabase = _defectsService.GetDefects().
                SingleOrDefault(o => o.Compare(firstDefect));
            Defect secondDefectFromDatabase = _defectsService.GetDefects().
                SingleOrDefault(o => o.Compare(secondDefect));
            Status firstStatusFromDatabase = _statusService.GetMultipleStatus().
                SingleOrDefault(o => o.Compare(firstStatus));
            Status secondStatusFromDatabase = _statusService.GetMultipleStatus().
                SingleOrDefault(o => o.Compare(secondStatus));
            ImageInDevice firstImageInDeviceFromDatabase = _imageInDevicesService.
                GetImageInDevices().SingleOrDefault(o => o.Compare(firstImageInDevice));
            ImageInDevice secondImageInDeviceFromDatabase = _imageInDevicesService.
                GetImageInDevices().SingleOrDefault(o => o.Compare(secondImageInDevice));
            Assert.NotNull(firstDefectFromDatabase);
            Assert.NotNull(secondDefectFromDatabase);
            Assert.NotNull(firstStatusFromDatabase);
            Assert.NotNull(secondStatusFromDatabase);
            Assert.NotNull(firstImageInDeviceFromDatabase);
            Assert.NotNull(secondImageInDeviceFromDatabase);
            myConstructionSite.MyDefects = new List<MyDefect>
            {
                new MyDefect
                {
                    SingleDefect = firstDefectFromDatabase,
                    MyMultipleStatus = new List<MyStatus>
                    {
                        new MyStatus
                        {
                            SingleStatus = firstStatusFromDatabase,
                            ImagesInDevice = new List<ImageInDevice> {firstImageInDeviceFromDatabase}
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Defect updatedDefect = _defectsService.GetDefect(secondDefectFromDatabase.Id);
            Status updatedStatus = _statusService.GetStatus(secondStatusFromDatabase.Id);
            ImageInDevice updatedImageInDevice = _imageInDevicesService.GetImageInDevice(
                secondImageInDeviceFromDatabase.Id);
            Assert.NotNull(updatedDefect);
            Assert.NotNull(updatedStatus);
            Assert.NotNull(updatedImageInDevice);
            Assert.IsFalse(updatedDefect.IsActive);
            Assert.IsFalse(updatedStatus.IsActive);
            Assert.IsFalse(updatedImageInDevice.IsActive);
            _defectsService.DeleteDefect(firstDefectFromDatabase.Id);
            _defectsService.DeleteDefect(secondDefectFromDatabase.Id);
            _statusService.DeleteStatus(firstStatusFromDatabase.Id);
            _statusService.DeleteStatus(secondStatusFromDatabase.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDeviceFromDatabase.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDeviceFromDatabase.Id);
            _employeesService.DeleteEmployee(employee.Id);
        }

        [Test]
        public void TestIfControllerDoesCrudOperationsWhenStatusDiffer()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect defect = GetFakeDefect(constructionSite.CreatorId, constructionSite.Id);
            Status firstStatus = GetFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = GetFakeStatus(defect.CreatorId + 1, defect.Id);
            ImageInDevice firstImageInDevice = GetFakeImageInDevice(1, firstStatus.Id);
            ImageInDevice secondImageInDevice = GetFakeImageInDevice(2, secondStatus.Id);
            Employee employee = PostFakeEmployee(constructionSite.Id, "1");
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                Employees = new List<Employee> {employee},
                SingleConstructionSite = constructionSite,
                SingleLocation = GetFakeLocation("1"),
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = defect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = firstStatus,
                                ImagesInDevice = new List<ImageInDevice> {firstImageInDevice}
                            },
                            new MyStatus
                            {
                                SingleStatus = secondStatus,
                                ImagesInDevice = new List<ImageInDevice> {secondImageInDevice }
                            }
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Defect firstDefectFromDatabase = _defectsService.GetDefects().Single(
                o => o.Compare(defect));
            Status firstStatusFromDatabase = _statusService.GetMultipleStatus().
                Single(o => o.Compare(firstStatus));
            Status secondStatusFromDatabase = _statusService.GetMultipleStatus().
                Single(o => o.Compare(secondStatus));
            ImageInDevice firstImageInDeviceFromDatabase = _imageInDevicesService
                .GetImageInDevices().Single(o => o.Compare(firstImageInDevice));
            ImageInDevice secondImageInDeviceFromDatabase = _imageInDevicesService.
                GetImageInDevices().Single(o => o.Compare(secondImageInDevice));
            Assert.NotNull(firstDefectFromDatabase);
            Assert.NotNull(firstStatusFromDatabase);
            Assert.NotNull(secondStatusFromDatabase);
            Assert.NotNull(firstImageInDeviceFromDatabase);
            Assert.NotNull(secondImageInDeviceFromDatabase);
            myConstructionSite.MyDefects = new List<MyDefect>
            {
                new MyDefect
                {
                    SingleDefect = firstDefectFromDatabase,
                    MyMultipleStatus = new List<MyStatus>
                    {
                        new MyStatus
                        {
                            SingleStatus = firstStatusFromDatabase,
                            ImagesInDevice = new List<ImageInDevice> {firstImageInDeviceFromDatabase}
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            Status updatedStatus = _statusService.GetStatus(secondStatusFromDatabase.Id);
            ImageInDevice updatedImageInDevice = _imageInDevicesService.GetImageInDevice(
                secondImageInDeviceFromDatabase.Id);
            Assert.NotNull(updatedStatus);
            Assert.NotNull(updatedImageInDevice);
            Assert.IsFalse(updatedStatus.IsActive);
            Assert.IsFalse(updatedImageInDevice.IsActive);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _defectsService.DeleteDefect(firstDefectFromDatabase.Id);
            _statusService.DeleteStatus(firstStatusFromDatabase.Id);
            _statusService.DeleteStatus(secondStatusFromDatabase.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDeviceFromDatabase.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDeviceFromDatabase.Id);
            _employeesService.DeleteEmployee(employee.Id);
        }

        [Test]
        public void TestIfControllerDoesCrudWhenImageInDevicesDiffer()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Employee employee = PostFakeEmployee(constructionSite.Id, "1");
            Defect defect = PostFakeDefect(constructionSite.CreatorId, constructionSite.Id);
            Status status = PostFakeStatus(defect.CreatorId, defect.Id);
            ImageInDevice firstImageInDevice = GetFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = GetFakeImageInDevice(2, status.Id);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                Employees = new List<Employee> {employee},
                SingleConstructionSite = constructionSite,
                SingleLocation = GetFakeLocation("1"),
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = defect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = status,
                                ImagesInDevice = new List<ImageInDevice>
                                {
                                    firstImageInDevice,
                                    secondImageInDevice
                                }
                            }
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            ImageInDevice firstImageInDeviceFromDatabase =
                _imageInDevicesService.GetImageInDevices().Single(
                    o => o.Compare(firstImageInDevice));
            ImageInDevice secondImageInDeviceFromDatabase =
                _imageInDevicesService.GetImageInDevices().Single(
                    o => o.Compare(secondImageInDevice));
            Assert.NotNull(firstImageInDeviceFromDatabase);
            Assert.NotNull(secondImageInDeviceFromDatabase);
            myConstructionSite.MyDefects = new List<MyDefect>
            {
                new MyDefect
                {
                    SingleDefect = defect,
                    MyMultipleStatus = new List<MyStatus>
                    {
                        new MyStatus
                        {
                            SingleStatus = status,
                            ImagesInDevice = new List<ImageInDevice> {firstImageInDevice}
                        }
                    }
                }
            };
            controller.UpdateConstructionSite(myConstructionSite);
            ImageInDevice updatedImageInDevice = _imageInDevicesService.GetImageInDevice(
                secondImageInDeviceFromDatabase.Id);
            Assert.NotNull(updatedImageInDevice);
            Assert.IsFalse(updatedImageInDevice.IsActive);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _defectsService.DeleteDefect(defect.Id);
            _statusService.DeleteStatus(status.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDeviceFromDatabase.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDeviceFromDatabase.Id);
            _employeesService.DeleteEmployee(employee.Id);
        }

        [Test]
        [Category("problematic")]
        public void TestIfStatusInheritsDefectIdOnUpdate()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect defect = GetFakeDefect("1", constructionSite.Id);
            Status firstStatus = GetFakeStatus("1", defect.Id);
            Employee employee = GetFakeEmployee(constructionSite.Id, "1");
            MyConstructionSite site = new MyConstructionSite
            {
                Employees = new List<Employee> {employee},
                SingleConstructionSite = constructionSite,
                SingleLocation = GetFakeLocation("1"),
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = defect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = firstStatus
                            }
                        }
                    }
                }
            };
            ResponseMessageResult result = controller.UpdateConstructionSite(site);
            Defect defectFromDatabase = _defectsService.GetDefects().FirstOrDefault(o => o.Compare(defect));
            ConstructionSite constructionSiteFromDatabase =
                _constructionSitesService.GetConstructionSite(constructionSite.Id);
            Status statusFromDatabase = _statusService.GetMultipleStatus()
                .Single(o => o.Compare(firstStatus));
            Assert.NotNull(statusFromDatabase);
            Assert.NotNull(constructionSiteFromDatabase);
            Assert.NotNull(defectFromDatabase);
            bool matchFound = false;
            foreach (MyDefect myDefect in site.MyDefects)
            {
                if (myDefect.MyMultipleStatus.
                    Any(myStatus => myDefect.SingleDefect.Id != 0 
                    && myStatus.SingleStatus.DefectId != 0 
                    && myDefect.SingleDefect.Id == myStatus.SingleStatus.DefectId))
                {
                    matchFound = true;
                }
            }
            Assert.IsTrue(matchFound);
            _constructionSitesService.DeleteConstructionSite(constructionSiteFromDatabase.Id);
            _defectsService.DeleteDefect(defect.Id);
            _employeesService.DeleteEmployee(employee.Id);
            _statusService.DeleteStatus(statusFromDatabase.Id);
        }

        [Test]
        [Category("problematic")]
        public void TestIfControllerUploadMethodValidatesLocation()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite mySite = GetFakeMyConstructionSite();
            Location fakeLocation = GetFakeLocation("");
            mySite.SingleLocation = fakeLocation;
            ObjectValidationError<Location> expectedError = new ObjectValidationError<Location>
            {
                Object = fakeLocation,
                ErrorList = new List<string> { LocationsService.CreatorIdNotSpecified}
            };
            ConstructionSite siteThatShouldNotExist = _constructionSitesService.GetConstructionSites().
                FirstOrDefault(o => o.Compare(mySite.SingleConstructionSite));
            ObjectValidationError<Location> actualError =
                ConvertResponseToObject<ObjectValidationError<Location>>(
                    controller.AddConstructionSite(mySite));
            Assert.IsNull(siteThatShouldNotExist);
            Assert.IsTrue(expectedError.Compare(actualError));
        }

        [Test]
        public void TestIfControllerUpdateMethodHandlesEmptyMyDefects()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite mySite = GetFakeMyConstructionSite();
            mySite.MyDefects = null;
            string receivedError = ConvertResponseToObject<List<string>>(
                controller.UpdateConstructionSite(mySite)).FirstOrDefault();
            Assert.IsTrue(MyConstructionSiteValidator.DefectsNotSpecified.Equals
                (receivedError));
        }
        
        [Test]
        public void TestIfControllerUpdateMethodHandlesEmptyMyDefectsParameters()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite constructionSite = GetFakeMyConstructionSite();
            constructionSite.Employees = GetFakeEmployees(1, "1");
            MyDefect emptyMyDefect = new MyDefect();
            MyDefect defectWithoutMultipleStatuses = new MyDefect
            {
                SingleDefect = GetFakeDefect("1", 1)
            };
            MyDefect defectWithoutSingleDefect = new MyDefect
            {
                MyMultipleStatus = GetFakeMyStatuses()
            };
            List<MyDefect> myDefects = new List<MyDefect>
            {
                emptyMyDefect,
                defectWithoutSingleDefect,
                defectWithoutMultipleStatuses
            };
            constructionSite.MyDefects = myDefects;
            List<ObjectValidationError<MyDefect>> expectedErrors = new List<ObjectValidationError<MyDefect>>
            {
                new ObjectValidationError<MyDefect>
                {
                    Object = emptyMyDefect,
                    ErrorList = new List<string>
                    {
                        MyConstructionSiteValidator.DefectNotSpecified,
                        MyConstructionSiteValidator.StatusesNotSpecified
                    }
                },
                new ObjectValidationError<MyDefect>
                {
                    Object = defectWithoutMultipleStatuses,
                    ErrorList = new List<string>
                    {
                        MyConstructionSiteValidator.StatusesNotSpecified
                    }
                },
                new ObjectValidationError<MyDefect>
                {
                    Object = defectWithoutSingleDefect,
                    ErrorList = new List<string>
                    {
                        MyConstructionSiteValidator.DefectNotSpecified
                    }
                }
            };
            List<ObjectValidationError<MyDefect>> actualErrors =
                ConvertResponseToObject<List<ObjectValidationError<MyDefect>>>(
                    controller.UpdateConstructionSite(constructionSite));
            Assert.IsTrue(expectedErrors.Where(e2 => !actualErrors.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0);
        }

        [Test]
        public void TestIfUpdateMethodHandlesEmptyStatusFields()
        {
            MyConstructionSitesController controller = InitializeControllerForTests();
            MyConstructionSite myConstructionSite = GetFakeMyConstructionSite();
            myConstructionSite.Employees = GetFakeEmployees(1, "1");
            MyStatus emptyMyStatus = new MyStatus();
            MyStatus myStatusWithoutSingleStatus = new MyStatus
            {
                ImagesInDevice = GetFakeImageInDevices(1)
            };
            MyStatus myStatusWithoutImagesInDevice = new MyStatus
            {
                SingleStatus = GetFakeStatus("1", 1)
            };
            List<MyStatus> myMultipleStatus = new List<MyStatus>
            {
                emptyMyStatus,
                myStatusWithoutSingleStatus,
                myStatusWithoutImagesInDevice
            };
            List<MyDefect> myDefects = new List<MyDefect>
            {
                new MyDefect
                {
                    SingleDefect = GetFakeDefect("1", 1),
                    MyMultipleStatus = myMultipleStatus
                }
            };
            myConstructionSite.MyDefects = myDefects;
            List<ObjectValidationError<MyStatus>> expectedErrors = new List<ObjectValidationError<MyStatus>>
            {
                new ObjectValidationError<MyStatus>
                {
                    Object = emptyMyStatus,
                    ErrorList = new List<string>
                    {
                        MyConstructionSiteValidator.StatusNotSpecified,
                    }
                },
                new ObjectValidationError<MyStatus>
                {
                    Object = myStatusWithoutSingleStatus,
                    ErrorList = new List<string>
                    {
                        MyConstructionSiteValidator.StatusNotSpecified
                    }
                }
            };
            List<ObjectValidationError<MyStatus>> actualErrors =
                ConvertResponseToObject<List<ObjectValidationError<MyStatus>>>(
                    controller.UpdateConstructionSite(myConstructionSite));
            Assert.IsTrue(expectedErrors.Where(e2 => !actualErrors.Any(
                   e1 => e1.Compare(e2))).ToList().Count == 0);

        }

        private MyConstructionSitesController InitializeControllerForTests()
        {
            HttpConfiguration config = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/user/1337");
            config.Routes.MapHttpRoute("Default", "api/{controller}/{id}");
            MyConstructionSitesController controller = new MyConstructionSitesController
            {
                Request = request
            };
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            return controller;
        }

    }
}