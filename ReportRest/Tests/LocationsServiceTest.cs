﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class LocationsServiceTest : TestUtilities
    {
        private readonly LocationsService _service = new LocationsService();
        [Test]
        public void TestIfServiceValidatesLocation()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Location location = GetFakeLocation("");
            location.ConstructionSiteId = 0;
            location.Description = null;
            List<string> errorList = _service.PostLocation(location);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 3 && 
                errorList.Contains(LocationsService.CreatorIdNotSpecified) && 
                errorList.Contains(LocationsService.DescriptionNotSpecified) &&
                errorList.Contains(LocationsService.ConstructionSiteIdNotSpecified) &&
                initialDatabaseSize == databaseSizeAfterCreation);
        }

        [Test]
        public void TestIfServiceDeniesAddingTwoSameLocations()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Location location = PostFakeLocation();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.PostLocation(location);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _service.DeleteLocation(location.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize, 
                    databaseSizeAfterCreation, databaseSizeAfterDeletion) && 
                databaseSizeAfterAttempt == databaseSizeAfterCreation);
        }

        [Test]
        public void TestIfServicePostsAndDeletesSameLocations()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Location location = PostFakeLocation();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DeleteLocation(location.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDisablesLocation()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Location location = PostFakeLocation();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DisableLocation(location.Id);
            Location locationFromDatabase = _service.GetLocation(location.Id);
            _service.DeleteLocation(locationFromDatabase.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                location.Compare(locationFromDatabase));
        }

        private Location PostFakeLocation()
        {
            Location location = GetFakeLocation("1");
            _service.PostLocation(location);
            return _service.GetLocations().FirstOrDefault(o => o.Compare(location));
        }

        private int GetCurrentDatabaseSize()
        {
            return _service.GetLocations().Count;
        }

    }
}
