﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Models;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class DefectsServiceTest : TestUtilities
    {

        [Test]
        public void TestIfServiceValidatesMandatoryDefectFields()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Defect defect = new Defect
            {
                ConstructionSiteId = 0,
                CreatorId = "",
                IsActive = true,
                IsFixed = false
            };
            List<string> errorList = _defectsService.PostDefect(defect);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 3 &&
                errorList.Contains(DefectsService.ConstructionSiteIdNotSpecified) &&
                errorList.Contains(DefectsService.CreatorIdNotSpecified) &&
                errorList.Contains(DefectsService.DateNotSpecified) &&
                initialDatabaseSize == databaseSizeAfterAttempt);
        }

        [Test]
        [Category("problematic")]
        public void TestIfServicePostsAndDeletesDefect()
        {
            int databaseSize = GetCurrentDatabaseSize();
            Defect defect = PostFakeDefect("1", 7);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _defectsService.DeleteDefect(defect.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(databaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDeniesAddingSameDefects()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Defect defect = PostFakeDefect("2", 1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            List<string> errorList = _defectsService.PostDefect(defect);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _defectsService.DeleteDefect(defect.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                errorList.Count == 1 &&
                errorList.Contains(DefectsService.DefectAlreadyExist) &&
                databaseSizeAfterCreation == databaseSizeAfterAttempt);
        }

        [Test]
        public void TestIfServiceDeniesUpdatingDeletedDefect()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Defect defect = PostFakeDefect("1", 1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _defectsService.DeleteDefect(defect.Id);
            int sizeAfterDeletion = GetCurrentDatabaseSize();
            List<string> errorsList = _defectsService.PutDefect(defect.Id, defect);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, sizeAfterDeletion) &&
                databaseSizeAfterAttempt == initialDatabaseSize &&
                errorsList.Count == 1 &&
                errorsList.Contains(DefectsService.DefectNotExist));
        }

        [Test]
        [Category("problematic")]
        public void TestIfServiceDisablesDefect()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Defect defect = PostFakeDefect("1",6);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _defectsService.DisableDefect(defect.Id);
            Defect defectFromDatabase = _defectsService.GetDefect(defect.Id);
            _defectsService.DeleteDefect(defectFromDatabase.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(
                initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                defectFromDatabase.IsActive == false);
        }

        [Test]
        [Category("problematic")]
        public void TestIfServiceMarksDefectAsFixed()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Defect defect = PostFakeDefect("1",5);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _defectsService.MarkDefectAsFixed(defect.Id);
            Defect defectFromDatabase = _defectsService.GetDefect(defect.Id);
            _defectsService.DeleteDefect(defectFromDatabase.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(
                initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion));
            Assert.IsTrue(defectFromDatabase.IsFixed);
        }

        [Test]
        public void TestIfServiceReturnsNewDefectListIfPayloadContainsNewDefects()
        {
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect firstDefect = PostFakeDefect("1", constructionSite.Id);
            Defect secondDefect = PostFakeDefect("2", constructionSite.Id);
            Defect thirdDefect = GetFakeDefect("3", constructionSite.Id);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = GetFakeMyStatuses()
                    },
                    new MyDefect
                    {
                        SingleDefect = secondDefect,
                        MyMultipleStatus = GetFakeMyStatuses()
                    },
                    new MyDefect
                    {
                        SingleDefect = thirdDefect,
                        MyMultipleStatus = GetFakeMyStatuses()
                    }
                }
            };
            List<Defect> newDefectsList = _defectsService.GetNewPayloadDefects(myConstructionSite);
            Assert.IsTrue(newDefectsList.Count == 1 && thirdDefect.Compare(newDefectsList.FirstOrDefault(o => o.Compare(thirdDefect))));
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _defectsService.DeleteDefect(firstDefect.Id);
            _defectsService.DeleteDefect(secondDefect.Id);
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfNoNewDefectsWerePresent()
        {
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect firstDefect = PostFakeDefect("1", constructionSite.Id);
            firstDefect.ConstructionSiteId = constructionSite.Id;
            Defect secondDefect = PostFakeDefect("2", constructionSite.Id);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = GetFakeMyStatuses()
                    },
                    new MyDefect
                    {
                        SingleDefect = secondDefect,
                        MyMultipleStatus = GetFakeMyStatuses()
                    }
                }
            };
            List<Defect> newDefects = _defectsService.GetNewPayloadDefects(myConstructionSite);
            Assert.IsTrue(newDefects.Count == 0);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _defectsService.DeleteDefect(firstDefect.Id);
            _defectsService.DeleteDefect(secondDefect.Id);
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfAllOldDefectsWereIncludedInPayload()
        {
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect firstDefect = PostFakeDefect(constructionSite.CreatorId, constructionSite.Id);
            Defect secondDefect = PostFakeDefect(constructionSite.CreatorId + 1, constructionSite.Id);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = null
                    },
                    new MyDefect
                    {
                        SingleDefect = secondDefect,
                        MyMultipleStatus = null
                    }
                }
            };
            List<Defect> defects = _defectsService.GetOldDefectsNotIncludedInPayload(myConstructionSite);
            Assert.IsTrue(defects.Count == 0);
        }

        [Test]
        public void TestIfServiceReturnsListOfOldDefectsThatWereNotPresentInPayload()
        {
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Defect firstDefect = PostFakeDefect(constructionSite.CreatorId, constructionSite.Id);
            Defect secondDefect = PostFakeDefect(constructionSite.CreatorId + 1, constructionSite.Id);
            List<MyStatus> myStatuses = GetFakeMyStatuses();
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = firstDefect,
                        MyMultipleStatus = myStatuses
                    },
                }
            };
            List<Defect> notIncludedOldDefects =
                _defectsService.GetOldDefectsNotIncludedInPayload(myConstructionSite);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            _defectsService.DeleteDefect(firstDefect.Id);
            _defectsService.DeleteDefect(secondDefect.Id);
            Assert.IsTrue(notIncludedOldDefects.Count == 1 &&
                secondDefect.Compare(notIncludedOldDefects.FirstOrDefault(o => o.Compare(secondDefect))));
        }

        [Test]
        public void TestIfServiceDistinguishesNewDefects()
        {
            Defect firstOldDefect = GetFakeDefect("1",1);
            Defect secondOldDefect = GetFakeDefect("2",1);
            List<Defect> oldDefects = new List<Defect>
            {
                firstOldDefect,
                secondOldDefect
            };
            Defect firstNewDefect = GetFakeDefect("3",1);
            firstNewDefect.ConstructionSiteId = firstOldDefect.ConstructionSiteId + 1;
            List<Defect> payload = oldDefects.Concat(new List<Defect> {firstNewDefect}).ToList();
            Assert.IsTrue(_defectsService.GetListOfNewDefects(oldDefects, payload)
                .FirstOrDefault().Compare(firstNewDefect));

        }

        private int GetCurrentDatabaseSize()
        {
            return _defectsService.GetDefects().Count;
        }

    }
}
