﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ImagesServiceTest :  TestUtilities
    {

        private readonly ImagesService _service = new ImagesService();

        [Test]
        public void TestIfServiceValidatesImage()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Image image = new Image
            {
                StatusId = 0,
                PhotoInBytes = null,
                IsActive = true
            };
            List<string> errorList = _service.PostImage(image);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 3 &&
                errorList.Contains(ImagesService.ImageInBytesNotSpecified) &&
                errorList.Contains(ImagesService.StatusIdNotSpecified) &&
                errorList.Contains(ImagesService.DateNotSpecified) &&
                initialDatabaseSize == databaseSizeAfterAttempt);
        }

        [Test]
        [Category("problematic")]
        public void TestIfServicePostsAndDeletesImage()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Image image = PostFakeImage();
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _service.DeleteImage(image.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize,
                    databaseSizeAfterAttempt, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDeniesToPostTwoSameImages()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Image image = PostFakeImage();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            List<string> errorsList = _service.PostImage(image);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _service.DeleteImage(image.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                errorsList.Count == 1 &&
                errorsList.Contains(ImagesService.ImageAlreadyExists) &&
                databaseSizeAfterAttempt == databaseSizeAfterCreation);
        }

        [Test]
        public void TestIfServiceDeniesToUpdateDeletedImage()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Image image = PostFakeImage();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DeleteImage(image.Id);
            int sizeAfterDeletion = GetCurrentDatabaseSize();
            List<string> errorList = _service.PutImage(image.Id, image);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, sizeAfterDeletion) &&
                initialDatabaseSize == databaseSizeAfterAttempt && 
                errorList.Count == 1 && 
                errorList.Contains(ImagesService.ImageNotExist));
        }

        [Test]
        [Category("problematic")]
        public void TestIfServiceDisablesImage()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            Image image = PostFakeImage();
            int sizeAfterCreation = GetCurrentDatabaseSize();
            _service.DisableImage(image.Id);
            Image imageFromDatabase = _service.GetImage(image.Id);
            _service.DeleteImage(imageFromDatabase.Id);
            int sizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, sizeAfterCreation, sizeAfterDeletion) &&
                imageFromDatabase.IsActive == false);
        }

        private int GetCurrentDatabaseSize()
        {
            return _service.GetImages().Count;
        }

        private Image PostFakeImage()
        {
            Image image = GetFakeImage(1, InitialLength, InitialValue);
            _service.PostImage(image);
            return _service.GetImages().FirstOrDefault(o => o.Compare(image));
        }

    }
}
