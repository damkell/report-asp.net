﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Models;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class StatusServiceTest : TestUtilities
    {

        [Test]
        public void TestIfServiceValidatesAllFields()
        {
            int initialDatabaseSize = CurrentDatabaseSize();
            Status status = GetFakeStatus("", 1);
            status.DefectId = 0;
            status.ShortDescription = null;
            List<string> errorList = _statusService.PostStatus(status);
            int databaseSizeAfterAttempt = CurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 3 &&
                errorList.Contains(StatusService.CreatorIdNotSpecified) &&
                errorList.Contains(StatusService.DefectIdNotSpecified) &&
                errorList.Contains(StatusService.ShortDescriptionNotSpecified) &&
                initialDatabaseSize == databaseSizeAfterAttempt);
        }

        [Test]
        public void TestIfServicePostsAndDeletesStatus()
        {
            int initialDatabaseSize = CurrentDatabaseSize();
            Status status = PostFakeStatus("1",1);
            int databaseSizeAfterAttempt = CurrentDatabaseSize();
            _statusService.DeleteStatus(status.Id);
            int databaseSizeAfterDeletion = CurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize, 
                    databaseSizeAfterAttempt, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDeniesAddingTwoSameStatus()
        {
            int initialDatabaseSize = CurrentDatabaseSize();
            Status status = PostFakeStatus("1",1);
            int databaseSizeAfterCreation = CurrentDatabaseSize();
            List<string> errorList = _statusService.PostStatus(status);
            int databaseSizeAfterIllegalAttempt = CurrentDatabaseSize();
            _statusService.DeleteStatus(status.Id);
            int databaseSizeAfterDeletion = CurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize,
                databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                databaseSizeAfterCreation == databaseSizeAfterIllegalAttempt &&
                errorList.Count == 1 &&
                errorList.Contains(StatusService.StatusAlreadyExist));
        }

        [Test]
        public void TestIfServiceDeniesUpdatingNonExistingStatus()
        {
            int initialDatabaseSize = CurrentDatabaseSize();
            Status status = GetFakeStatus("1", 2);
            List<string> errorList = _statusService.PutStatus(status.Id, status);
            int databaseSizeAfterIllegalAtempt = CurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 1 &&
                errorList.Contains(StatusService.StatusNotExist) &&
                initialDatabaseSize == databaseSizeAfterIllegalAtempt);
        }

        [Test]
        [Category("problematic")]
        public void TestIfServiceDisablesStatus()
        {
            int initialDatabaseSize = CurrentDatabaseSize();
            Status status = PostFakeStatus("1",1);
            int databaseSizeAfterCreation = CurrentDatabaseSize();
            _statusService.DisableStatus(status.Id);
            Status statusFromDatabase = _statusService.GetStatus(status.Id);
            _statusService.DeleteStatus(statusFromDatabase.Id);
            int finalDatabaseSize = CurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize,
                    databaseSizeAfterCreation, finalDatabaseSize));
            Assert.IsFalse(statusFromDatabase.IsActive);
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfNoNewStatusesWereGiven()
        {
            Defect defect = PostFakeDefect("1", 1);
            Status firstStatus = PostFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = PostFakeStatus(defect.CreatorId + 1, defect.Id);
            List<ImageInDevice> imageInDevices = GetFakeImageInDevices(1);
            MyDefect myDefect = new MyDefect
            {
                SingleDefect = defect,
                MyMultipleStatus = new List<MyStatus>
                {
                    new MyStatus
                    {
                        SingleStatus = firstStatus,
                        ImagesInDevice = imageInDevices
                    },
                    new MyStatus
                    {
                        SingleStatus = secondStatus,
                        ImagesInDevice = imageInDevices
                    },
                }
            };
            List<Status> newStatuses = _statusService.GetNewPayloadStatuses(myDefect);
            Assert.IsTrue(newStatuses.Count == 0);
            _defectsService.DeleteDefect(defect.Id);
            _statusService.DeleteStatus(firstStatus.Id);
            _statusService.DeleteStatus(secondStatus.Id);
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfPayloadContainsAllOldStatuses()
        {
            Defect defect = PostFakeDefect("1", 1);
            Status firstStatus = PostFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = PostFakeStatus(defect.CreatorId + 1, defect.Id);
            MyDefect myDefect = new MyDefect
            {
                SingleDefect = defect,
                MyMultipleStatus = new List<MyStatus>
                {
                    new MyStatus
                    {
                        SingleStatus = firstStatus,
                        ImagesInDevice = GetFakeImageInDevices(1)
                    },
                    new MyStatus
                    {
                        SingleStatus = secondStatus,
                        ImagesInDevice = GetFakeImageInDevices(1)
                    },
                }
            };
            List<Status> notMentionedOldStatuses = _statusService.StatusesFromDefectNotInPayload(myDefect);
            _defectsService.DeleteDefect(defect.Id);
            _statusService.DeleteStatus(firstStatus.Id);
            _statusService.DeleteStatus(secondStatus.Id);
            Assert.IsTrue(notMentionedOldStatuses.Count == 0);
        }

        [Test]
        public void TestIfServiceReturnsListOfNotM()
        {
            Defect defect = PostFakeDefect("1", 1);
            Status firstStatus = PostFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = PostFakeStatus(defect.CreatorId + 1, defect.Id);
            List<ImageInDevice> imagesInDevices = GetFakeImageInDevices(1);
            MyDefect myDefect = new MyDefect
            {
                SingleDefect = defect,
                MyMultipleStatus = new List<MyStatus>
                {
                    new MyStatus
                    {
                        SingleStatus = firstStatus,
                        ImagesInDevice = imagesInDevices
                    }
                }
            };
            List<Status> notMentionedOldStatuses =
                _statusService.StatusesFromDefectNotInPayload(myDefect);
            _defectsService.DeleteDefect(defect.Id);
            _statusService.DeleteStatus(firstStatus.Id);
            _statusService.DeleteStatus(secondStatus.Id);
            Assert.IsTrue(notMentionedOldStatuses.Count == 1 &&
                secondStatus.Compare(notMentionedOldStatuses.FirstOrDefault(o => o.Compare(secondStatus))));
        }

        private int CurrentDatabaseSize()
        {
            return _statusService.GetMultipleStatus().Count;
        }

    }
}
