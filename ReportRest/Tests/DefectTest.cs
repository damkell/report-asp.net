﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class DefectTest : TestUtilities
    {

        [Test]
        public void TestIfDefectCompareMethodReturnsTrueInSame()
        {
            Defect defect = GetFakeDefect("1", 1);
            Assert.IsTrue(defect.Compare(defect));
        }

        [Test]
        public void TestIfDefectCompareMethodReturnsFalseInDifferent()
        {
            Defect defect = GetFakeDefect("1", 1);
            Defect defect2 = GetFakeDefect("2", 1);
            Assert.IsFalse(defect.Compare(defect2));
        }

    }
}
