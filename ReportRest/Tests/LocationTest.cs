﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class LocationTest : TestUtilities
    {

        [Test]
        public void TestIfLocationCompareMethodReturnsTrueInSameLocations()
        {
            Location location = GetFakeLocation("1");
            Assert.IsTrue(location.Compare(location));
        }

        [Test]
        public void TestIfLocationCompareMethodReturnsFalseInDifferentLocations()
        {
            Location firstLocation = GetFakeLocation("1");
            Location secondLocation = GetFakeLocation("2");
            Assert.IsFalse(firstLocation.Compare(secondLocation));
        }

    }
}
