﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using ReportRest.Utilities;
using ReportRest.Validations;

namespace ReportRest.Tests
{
    [TestFixture]
    public class MyConstructionSiteServiceTest : TestUtilities
    {

        private readonly MyConstructionSiteService _myConstructionSiteService = new MyConstructionSiteService();

        

        [Test]
        public void TestIfServicePreventsPostingLocationWithNonExistingConstructionSiteId()
        {
            int notExistingId = GetIdOfNonExistingConstructionSite();
            Location location = GetFakeLocation("1");
            location.ConstructionSiteId = notExistingId;
            ObjectValidationError<Location> expectedError = new ObjectValidationError<Location>
            {
                Object = location,
                ErrorList = new List<string> {ConstructionSitesService.SiteNotExist}
            };
            ObjectValidationError<Location> actualError = _myConstructionSiteService
                .PostLocationAndIfItIsNotValidGetError(location);
            Assert.IsTrue(expectedError.Compare(actualError));
        }

        

        [Test]
        public void TestIfServiceAllowsToPostLocationWithValidConstructionSiteId()
        {
            ConstructionSite constructionSite = PostFakeConstructionSite("1");
            Location location = GetFakeLocation("1");
            location.ConstructionSiteId = constructionSite.Id;
            ObjectValidationError<Location> actualError = _myConstructionSiteService
                .PostLocationAndIfItIsNotValidGetError(
                    location);
            _constructionSitesService.DeleteConstructionSite(constructionSite.Id);
            Location locationFromDatabase = _locationsService.GetLocations()
                .FirstOrDefault(o => o.Compare(location));
            if (locationFromDatabase != null)
            {
                _locationsService.DeleteLocation(location.Id);
            }
            Assert.IsNull(actualError);
        }

        [Test]
        public void TestIfServiceReturnsListOfNewPayloadStatuses()
        {
            Defect defect = PostFakeDefect("1", 1);
            Status firstStatus = PostFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = PostFakeStatus(defect.CreatorId + 1, defect.Id);
            Status thirdStatus = GetFakeStatus(defect.CreatorId + 2, defect.Id);
            List<ImageInDevice> imageInDevices = GetFakeImageInDevices(1);
            MyDefect myDefect = new MyDefect
            {
                SingleDefect = defect,
                MyMultipleStatus = new List<MyStatus>
                {
                    new MyStatus
                    {
                        SingleStatus = firstStatus,
                        ImagesInDevice = imageInDevices
                    },
                    new MyStatus
                    {
                        SingleStatus = secondStatus,
                        ImagesInDevice = imageInDevices
                    },
                    new MyStatus
                    {
                        SingleStatus = thirdStatus,
                        ImagesInDevice = imageInDevices
                    }
                }
            };
            List<Status> newStatuses = _statusService.GetNewPayloadStatuses(myDefect);
            Assert.IsTrue(newStatuses.Count == 1 &&
                thirdStatus.Compare(newStatuses.FirstOrDefault(o => o.Compare(thirdStatus))));
            _defectsService.DeleteDefect(defect.Id);
            _statusService.DeleteStatus(firstStatus.Id);
            _statusService.DeleteStatus(secondStatus.Id);
            _statusService.DeleteStatus(thirdStatus.Id);
        }


    }
}