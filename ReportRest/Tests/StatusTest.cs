﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class StatusTest : TestUtilities
    {

        [Test]
        public void TestIfStatusCompareMethodReturnsTrueInSameStatus()
        {
            Status status = GetFakeStatus("1",1);
            Assert.IsTrue(status.Compare(status));
        }

        [Test]
        public void TestIfCompareMethodReturnsFalseInDifferentStatus()
        {
            Status firstStatus = GetFakeStatus("1",1);
            Status secondStatus = GetFakeStatus("2",2);
            Assert.IsFalse(firstStatus.Compare(secondStatus));
        }

    }
}
