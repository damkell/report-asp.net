﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Utilities;
using ReportRest.Validations;

namespace ReportRest.Tests
{
    [TestFixture]
    public class MyConstructionSitesValidatorTest : TestUtilities
    {

        [Test]
        public void TestIfValidatorReturnsErrorInEmployeeForeignKeyMismatch()
        {
            ConstructionSite constructionSite = GetFakeConstructionSite("1");
            constructionSite.Id = 5;
            Employee firstEmployee = GetFakeEmployee(constructionSite.Id, "1");
            Employee secondEmployee = GetFakeEmployee(constructionSite.Id + 1, "1");
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                Employees = new List<Employee> {firstEmployee, secondEmployee}
            };
            ObjectValidationError<Employee> expectedError = new ObjectValidationError<Employee>
            {
                Object = secondEmployee,
                ErrorList = new List<string> {MyConstructionSiteValidator.EmployeeForeignKeyMismatch}
            };
            List<ObjectValidationError<Employee>> actualError =
                MyConstructionSiteValidator.CheckForeignKeysInEmployees(myConstructionSite);
            Assert.NotNull(actualError);
            Assert.AreEqual(1, actualError.Count);
            Assert.IsTrue(expectedError.Compare(actualError.FirstOrDefault()));
        }

        [Test]
        public void TestIfValidatorReturnsErrorInDefectForeignKeyMismatch()
        {
            ConstructionSite constructionSite = GetFakeConstructionSite("1");
            constructionSite.Id = 5;
            Defect firstDefect = GetFakeDefect(constructionSite.CreatorId, constructionSite.Id);
            Defect secondDefect = GetFakeDefect(constructionSite.CreatorId, constructionSite.Id + 1);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                SingleConstructionSite = constructionSite,
                MyDefects = new List<MyDefect>
                {
                    new MyDefect {SingleDefect = firstDefect},
                    new MyDefect {SingleDefect = secondDefect}
                }
            };
            ObjectValidationError<Defect> expectedError = new ObjectValidationError<Defect>
            {
                Object = secondDefect,
                ErrorList = new List<string> {MyConstructionSiteValidator.DefectForeignKeyMismatch}
            };
            List<ObjectValidationError<Defect>> actualErrorList = MyConstructionSiteValidator
                .CheckForeignKeysInDefects(myConstructionSite);
            Assert.NotNull(actualErrorList);
            Assert.AreEqual(1, actualErrorList.Count);
            Assert.IsTrue(expectedError.Compare(actualErrorList.First()));
        }

        [Test]
        public void TestIfValidatorReturnsErrorInStatusForeignKeyMishmatch()
        {
            Defect defect = GetFakeDefect("1",1);
            defect.Id = 5;
            Status firstStatus = GetFakeStatus(defect.CreatorId, defect.Id);
            Status secondStatus = GetFakeStatus(defect.CreatorId, defect.Id + 1);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        SingleDefect = defect,
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus {SingleStatus = firstStatus},
                            new MyStatus {SingleStatus = secondStatus}
                        }
                    }
                }
            };
            ObjectValidationError<Status> expectedError = new ObjectValidationError<Status>
            {
                Object = secondStatus,
                ErrorList = new List<string> {MyConstructionSiteValidator.StatusForeignKeyMismatch}
            };
            List<ObjectValidationError<Status>> actualError =
                MyConstructionSiteValidator.CheckForeignKeysInStatuses(myConstructionSite);
            Assert.NotNull(actualError);
            Assert.AreEqual(1, actualError.Count);
            Assert.IsTrue(expectedError.Compare(actualError.FirstOrDefault()));
        }

        [Test]
        public void CheckIfValidatorReturnsErrorInImagesInDeviceForeignKeyMismatch()
        {
            Status status = GetFakeStatus("1", 1);
            status.Id = 5;
            ImageInDevice firstImageInDevice = GetFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = GetFakeImageInDevice(1, status.Id + 1);
            MyConstructionSite myConstructionSite = new MyConstructionSite
            {
                MyDefects = new List<MyDefect>
                {
                    new MyDefect
                    {
                        MyMultipleStatus = new List<MyStatus>
                        {
                            new MyStatus
                            {
                                SingleStatus = status,
                                ImagesInDevice = new List<ImageInDevice>
                                {
                                    firstImageInDevice, secondImageInDevice
                                }
                            }
                        }
                    }
                }
            };
            ObjectValidationError<ImageInDevice> expectedError = new ObjectValidationError<ImageInDevice>
            {
                Object = secondImageInDevice,
                ErrorList = new List<string> { MyConstructionSiteValidator.ImageInDeviceForeignKeyMismatch}
            };
            List<ObjectValidationError<ImageInDevice>> actualError =
                MyConstructionSiteValidator.CheckForeignKeysInImagesInDevice(myConstructionSite);
            Assert.NotNull(actualError);
            Assert.AreEqual(1, actualError.Count);
            Assert.IsTrue(expectedError.Compare(actualError.FirstOrDefault()));
        }

    }
}