﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ImageTest : TestUtilities
    {
        [Test]
        public void TestIfCompareMethodReturnsTrueIfSame()
        {
            Image image = GetFakeImage(1, InitialLength, InitialValue);
            Assert.IsTrue(image.Compare(image));
        }

        [Test]
        public void TestIfCompareMethodReturnsFalseIfByteArraysAreDifferent()
        {
            Image image = GetFakeImage(1, InitialLength, InitialValue);
            Image secondImage = GetFakeImage(1, SecondInitialValue, InitialLength);
            Assert.IsFalse(image.Compare(secondImage));
        }

    }
}
