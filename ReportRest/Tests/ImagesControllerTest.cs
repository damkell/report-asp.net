﻿using NUnit.Framework;
using ReportRest.Controllers;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Linq;
using ReportRest.Utilities;
using ReportRest.Validations;

namespace ReportRest.Tests
{
    [TestFixture]
    class ImagesControllerTest : TestUtilities
    {
        private readonly ImagesService _service = new ImagesService();

        [Test]
        public void TestIfControllerUploadMethodReturnsErrorInEmptyFields()
        {
            ImagesController controller = InitializeControllerForTests();
            MyImage myImage = new MyImage();
            List<string> emptyFieldsErrors =
                ConvertResponseToObject<List<string>>(
                    controller.UploadImage(myImage));
            Assert.IsTrue(emptyFieldsErrors.Count == 2 &&
                emptyFieldsErrors.Contains(ImagesController.ImageInDeviceMustBeSpecified) &&
                emptyFieldsErrors.Contains(ImagesController.ImageMustBeSpecified));
        }

        [Test]
        public void TestIfControllerValidatesImageInDevice()
        {
            ImagesController imagesController = InitializeControllerForTests();
            List<ImageInDevice> fakeImageInDevices = GetFakeImageInDevices(0);
            MyImage myImageToPass = new MyImage
            {
                SingleImage = GetFakeImage(1, InitialLength, InitialValue),
                ImageInDevices = fakeImageInDevices
            };
            List<string> errorList = new List<string> {ImageInDevicesService.DeviceIdNotSpecified};
            ObjectValidationError<ImageInDevice> expectedValidationError = new ObjectValidationError<ImageInDevice>
            {
                Object = fakeImageInDevices.FirstOrDefault(),
                ErrorList = errorList
            };
            List<ObjectValidationError<ImageInDevice>> actualErrorList =
                ConvertResponseToObject<List<ObjectValidationError<ImageInDevice>>>(
                imagesController.UploadImage(myImageToPass));
            Assert.IsTrue(actualErrorList.Count == 1 && 
                actualErrorList.FirstOrDefault(o => o.Compare(expectedValidationError)) != null);
        }

        [Test]
         public void TestIfControllerUploadsGetsAndDeletesImage()
         {
             ImagesController controller = InitializeControllerForTests();
            MyImage myImage = GetFakeMyImage(1, InitialLength, InitialValue);
             controller.UploadImage(myImage);
             Image imageWithId = _service.GetImages().FirstOrDefault(o => o.Compare(myImage.SingleImage));
            if (imageWithId != null)
            {
                Image imageFromController =
                     ConvertResponseToObject<Image>(controller.GetImage(imageWithId.Id));
                controller.DeleteImage(imageFromController.Id);
                string maybeError = 
                    ConvertResponseToObject<string>(controller.DeleteImage(imageFromController.Id));
                Assert.IsTrue(imageWithId.Compare(imageFromController) &&
                    maybeError == ImagesService.ImageNotExist);
            }
         }

         [Test]
         public void TestIfControllerHandlesNullInIds()
         {
             ImagesController controller = InitializeControllerForTests();
             List<int> idList = new List<int> {1, 2, 0};
             string maybeError = 
                ConvertResponseToObject<string>(controller.DeleteMultipleImages(idList));
             Assert.AreEqual(maybeError, ImagesController.IdCannotBeNull);
        }

        [Test]
         public void TestIfControllerDeletesMultipleImages()
         {
             ImagesController controller = InitializeControllerForTests();
             MyImage myImage = GetFakeMyImage(1, InitialLength, InitialValue);
             controller.UploadImage(myImage);
             MyImage myImage2 = GetFakeMyImage(2, InitialLength, InitialValue);
             controller.UploadImage(myImage2);
             Image firstImageFromDatabase = _service.GetImages().FirstOrDefault(o => o.Compare(myImage.SingleImage));
             Image secondImageFromDatabase = _service.GetImages().FirstOrDefault(o => o.Compare(myImage2.SingleImage));
            List<int> idsToDelete = null;
            if (firstImageFromDatabase != null && secondImageFromDatabase != null)
            {
                idsToDelete = new List<int> {firstImageFromDatabase.Id, secondImageFromDatabase.Id};
                controller.DeleteMultipleImages(idsToDelete);
                List<ObjectValidationError<int>> receivedErrors =
                    ConvertResponseToObject<List<ObjectValidationError<int>>>(
                        controller.GetMultipleImages(idsToDelete));
                foreach (ObjectValidationError<int> error in receivedErrors)
                {
                    if (idsToDelete.Contains(error.Object)
                        && error.ErrorList != null && error.ErrorList.Count == 1
                        && error.ErrorList.Contains(ImagesService.ImageNotExist))
                    {
                        idsToDelete.Remove(error.Object);
                    }
                }
            }
            Assert.IsTrue(idsToDelete != null && idsToDelete.Count == 0);
        }

        private ImagesController InitializeControllerForTests()
        {
                HttpConfiguration config = new HttpConfiguration();
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/user/1337");
                config.Routes.MapHttpRoute("Default", "api/{controller}/{id}");
                ImagesController controller = new ImagesController
                {
                    Request = request
                };
                controller.Request = request;
                controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
                return controller;
        }

    }
}
