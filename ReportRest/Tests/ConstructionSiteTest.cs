﻿using NUnit.Framework;
using ReportRest.Documents;
using System;
using System.Collections.Generic;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ConstructionSiteTest : TestUtilities
    {

        [Test]
        public void TestIfConstructionSiteCompareMethodReturnsTrueInSameObject()
        {
            ConstructionSite firstConstructionSite = new ConstructionSite
            {
                Id = 1,
                CreatorId = "1",
                CreatedAt = new DateTime(),
                IsActive = true
            };

            ConstructionSite secondConstructionSite = new ConstructionSite
            {
                Id = 1,
                CreatorId = "1",
                CreatedAt = new DateTime(),
                IsActive = true
            };

            Assert.IsTrue(firstConstructionSite.Compare(secondConstructionSite));

        }

        [Test]
        public void TestIfConstructionSiteCompareMethodReturnsFalseInDifferentObject()
        {
            ConstructionSite firstConstructionSite = new ConstructionSite
            {
                Id = 1,
                CreatorId = "1",
                CreatedAt = new DateTime(),
                IsActive = true
            };

            ConstructionSite secondConstructionSite = new ConstructionSite
            {
                CreatorId = "2",
                CreatedAt = new DateTime(),
                IsActive = true
            };

            Assert.IsFalse(firstConstructionSite.Compare(secondConstructionSite));
        }

    }
}
