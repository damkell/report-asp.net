﻿using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using ReportRest.Services;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    public class PendingEventsServiceTest : TestUtilities
    {
        private readonly PendingEventsService _pendingEventService = new PendingEventsService();
       
       [Test]
        public void TestIfServiceAllowsToPostEventWithoutInviterIdInRegistration()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            PendingEvent pendingEvent = GetFakePendingEvent("0");
            pendingEvent.EventType = PendingEvent.Event.Registration;
            List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(pendingEvent);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            PendingEvent pendingEventFromDatabase = _pendingEventService.GetPendingEvents()
                .FirstOrDefault(o => o.Compare(pendingEvent));
            if (pendingEventFromDatabase != null) _pendingEventService.DeletePendingEvent(pendingEventFromDatabase.Id);
            int finalDatabaseSize = GetCurrentDatabaseSize();
            Assert.IsTrue(errorList == null && 
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterAttempt, finalDatabaseSize));
        }

        [Test]
       public void TestIfServiceDeniesToPostAlreadyCreatedPendingEvent()
       {
           int initialDatabaseSize = GetCurrentDatabaseSize();
           PendingEvent pendingEvent = PostFakePendingEvent();
           int databaseSizeAfterCreation = GetCurrentDatabaseSize();
           List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(pendingEvent);
           int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
           _pendingEventService.DeletePendingEvent(pendingEvent.Id);
           int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
           Assert.IsTrue(errorList.ToArray().Length == 1 && 
               errorList.Contains(PendingEventsService.PendingEventAlreadyExist) &&
               DatabaseSizeCorrect(
                   initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
               databaseSizeAfterCreation == databaseSizeAfterAttempt);
       }

        [Test]
        public void TestIfServiceDeniesToPostBypassingInviterIdAndConstructionSiteIdWhenInviting()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            PendingEvent testEvent = GetFakePendingEvent("");
            testEvent.ConstructionSiteId = 0;
            testEvent.EventType = PendingEvent.Event.Invitation;
            List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(testEvent);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.AreEqual(2, errorList.ToArray().Length);
            Assert.IsTrue(errorList.ToArray().Length == 2);
            Assert.Contains(PendingEventsService.InviterIdNotSpecified, errorList);
            Assert.Contains(PendingEventsService.GroupIdNotSpecified, errorList);
            Assert.AreEqual(initialDatabaseSize, databaseSizeAfterAttempt);
        }

        [Test]
        public void TestIfServiceDeniesToPostBypassingInviterIdAndConstructionSiteIdWhenAdding()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            PendingEvent fakeEvent = GetFakePendingEvent("");
            fakeEvent.ConstructionSiteId = 0;
            fakeEvent.EventType = PendingEvent.Event.Addition;
            List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(fakeEvent);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.AreEqual(2, errorList.Count);
            Assert.Contains(PendingEventsService.InviterIdNotSpecified, errorList);
            Assert.Contains(PendingEventsService.GroupIdNotSpecified, errorList);
            Assert.AreEqual(initialDatabaseSize, databaseSizeAfterAttempt);
            Assert.AreEqual(initialDatabaseSize, databaseSizeAfterAttempt);
        }


        [Test]
        public void TestIfServiceDeniesToBypassHashWhenRegistering()
        {
            PendingEvent pendingEvent = GetFakePendingEvent("1");
            pendingEvent.EventType = PendingEvent.Event.Registration;
            pendingEvent.Hash = null;
            List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(pendingEvent);
            Assert.IsTrue(errorList.ToArray().Length == 1
                && errorList.Contains(PendingEventsService.HashNotSpecified));
        }
        [Test]
        public void TestIfServiceDeniesToBypassHashWhenInviting()
        {
            PendingEvent pendingEvent = GetFakePendingEvent("123456");
            pendingEvent.EventType = PendingEvent.Event.Invitation;
            pendingEvent.Hash = "";
            List<string> errorList = _pendingEventService.PostPendingEventAndGetErrors(pendingEvent);
            Assert.IsTrue(errorList.ToArray().Length == 1
                && errorList.Contains(PendingEventsService.HashNotSpecified));
        }

        [Test]
        public void TestIfServiceCreatesAndDeletesPendingEvent()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            PendingEvent pendingEvent = PostFakePendingEvent();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _pendingEventService.DeletePendingEvent(pendingEvent.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize,databaseSizeAfterCreation,databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDisablesPendingEvent()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            PendingEvent pendingEvent = PostFakePendingEvent();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _pendingEventService.DisablePendingEvent(pendingEvent);
            PendingEvent pendingEventFromDatabase = _pendingEventService.GetPendingEvent(pendingEvent.Id);
            _pendingEventService.DeletePendingEvent(pendingEvent.Id);
            int finalDatabaseSize = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize,databaseSizeAfterCreation,
                    finalDatabaseSize) && 
                pendingEventFromDatabase.IsActive == false);
        }

        private int GetCurrentDatabaseSize()
        {
            return _pendingEventService.GetPendingEvents().Count;
        }

        private PendingEvent PostFakePendingEvent()
        {
            PendingEvent pendingEvent = GetFakePendingEvent("1");
            _pendingEventService.PostPendingEventAndGetErrors(pendingEvent);
            return _pendingEventService.GetPendingEvents().FirstOrDefault(o => o.Compare(pendingEvent));
        }

    }
}
