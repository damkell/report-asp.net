﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ConstructionSitesServiceTest : TestUtilities
    {

        private readonly ConstructionSitesService _service = new ConstructionSitesService();

        [Test]
        public void CheckIfServiceValidatesAllMandatoryFields()
        {
            ConstructionSite constructionSite = new ConstructionSite
            {
                Id = 0,
                IsActive = true
            };
            List<string> errorList = _service.PostConstructionSite(constructionSite);
            Assert.IsTrue(errorList.ToArray().Length == 2 &&
                errorList.Contains(ConstructionSitesService.CreatorIdNotSpecified) &&
                errorList.Contains(ConstructionSitesService.DateNotSpecified));
        }

        [Test]
        public void TestIfServicePostsAndDeletesConstructionSiteToDatabase()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ConstructionSite constructionSite = PostFakeConstructionSiteAndGetReal();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DeleteConstructionSite(constructionSite.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceDeniesAddingSameConstructionSites()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ConstructionSite constructionSite = PostFakeConstructionSiteAndGetReal();
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            List<string> errorList = _service.PostConstructionSite(constructionSite);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _service.DeleteConstructionSite(constructionSite.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                ThisLengthAndContainsMessage(errorList, ConstructionSitesService.SiteAlreadyExists, 1) &&
                databaseSizeAfterCreation == databaseSizeAfterAttempt);
        }

        private ConstructionSite PostFakeConstructionSiteAndGetReal()
        {
            ConstructionSite constructionSite = new ConstructionSite
            {
                CreatorId = "2",
                CreatedAt = new DateTime(2011, 1, 1),
                IsActive = true
            };
            _service.PostConstructionSite(constructionSite);
            ConstructionSite constructionSiteFromDatabase = _service.GetConstructionSites()
                 .FirstOrDefault(o => o.Compare(constructionSite));
            return constructionSiteFromDatabase;
        }

        private int GetCurrentDatabaseSize()
        {
            return _service.GetConstructionSites().ToArray().Length;
        }

    }
}