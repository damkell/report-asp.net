﻿using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using ReportRest.Models;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    class ImageInDevicesServiceTest : TestUtilities
    {

        private readonly ImageInDevicesService _service = new ImageInDevicesService();

        [Test]
        public void TestIfServiceValidatesImageInDevicesMandatoryFields()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ImageInDevice image = new ImageInDevice
            {
                DeviceId = 0,
                ImageId = 0,
                ImageLocation = null,
                IsActive = true
            };
            List<string> errorList = _service.PostImageInDevice(image);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            Assert.IsTrue(errorList.Count == 3 &&
                errorList.Contains(ImageInDevicesService.DeviceIdNotSpecified) &&
                errorList.Contains(ImageInDevicesService.ImageLocationNotSpecified) &&
                errorList.Contains(ImageInDevicesService.DateNotSpecified) &&
                initialDatabaseSize == databaseSizeAfterAttempt);
        }

        [Test]
        [Category("problematic")]
        public void TestIfServicePostsAndDeletesImageInDevice()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ImageInDevice image = PostFakeImageInDevice(1, 1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DeleteImageInDevice(image.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion));
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfNoNewImageInDevicesWerePresent()
        {
            Status status = PostFakeStatus("1", 1);
            ImageInDevice firstImageInDevice = PostFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = PostFakeImageInDevice(2, status.Id);
            MyStatus myStatus = new MyStatus
            {
                SingleStatus = status,
                ImagesInDevice = new List<ImageInDevice>
                {
                    firstImageInDevice,
                    secondImageInDevice
                }
            };
            List<ImageInDevice> newItemsList = 
                _imageInDevicesService.GetNewPayloadImageInDevices(myStatus);
            _statusService.DeleteStatus(status.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDevice.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDevice.Id);
            Assert.IsTrue(newItemsList.Count == 0);
        }

        [Test]
        public void TestIfServiceReturnsListOfNotMentionedOldImagesInDevice()
        {
            Status status = PostFakeStatus("1", 1);
            ImageInDevice firstImageInDevice = PostFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = GetFakeImageInDevice(2, status.Id);
            MyStatus myStatus = new MyStatus
            {
                SingleStatus = status,
                ImagesInDevice = new List<ImageInDevice>
                {
                    secondImageInDevice
                }
            };
            List<ImageInDevice> missedItems =
                _imageInDevicesService.GetOldImageInDevicesNotIncludedInPayload(myStatus);
            _statusService.DeleteStatus(status.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDevice.Id);
            Assert.IsTrue(missedItems.Count == 1 &&
                firstImageInDevice.Compare(missedItems.FirstOrDefault(o => o.Compare(firstImageInDevice))));
        }

        [Test]
        public void TestIfServiceReturnsEmptyListIfAllOldImagesInDevicePresent()
        {
            Status status = PostFakeStatus("1", 1);
            ImageInDevice firstImageInDevice = PostFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = PostFakeImageInDevice(2, status.Id);
            ImageInDevice thirdImageInDevice = GetFakeImageInDevice(3, status.Id);
            MyStatus myStatus = new MyStatus
            {
                SingleStatus = status,
                ImagesInDevice = new List<ImageInDevice>
                {
                    firstImageInDevice,
                    secondImageInDevice,
                    thirdImageInDevice
                }
            };
            List<ImageInDevice> missedOldItems =
                _imageInDevicesService.GetOldImageInDevicesNotIncludedInPayload(myStatus);
            _statusService.DeleteStatus(status.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDevice.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDevice.Id);
            Assert.IsTrue(missedOldItems.Count == 0);
        }


        [Test]
        public void TestIfServiceReturnsListOfNewPayloadImageInDevices()
        {
            Status status = PostFakeStatus("1", 1);
            ImageInDevice firstImageInDevice = PostFakeImageInDevice(1, status.Id);
            ImageInDevice secondImageInDevice = PostFakeImageInDevice(2, status.Id);
            ImageInDevice thirdImageInDevice = GetFakeImageInDevice(3, status.Id);
            MyStatus myStatus = new MyStatus
            {
                SingleStatus = status,
                ImagesInDevice = new List<ImageInDevice>
                {
                    firstImageInDevice,
                    secondImageInDevice,
                    thirdImageInDevice
                }
            };
            List<ImageInDevice> newPayloadItems = _imageInDevicesService.GetNewPayloadImageInDevices(myStatus);
            _statusService.DeleteStatus(status.Id);
            _imageInDevicesService.DeleteImageInDevice(firstImageInDevice.Id);
            _imageInDevicesService.DeleteImageInDevice(secondImageInDevice.Id);
            Assert.IsTrue(newPayloadItems.Count == 1 &&
                thirdImageInDevice.Compare(newPayloadItems.FirstOrDefault(o => o.Compare(thirdImageInDevice))));
        }

        [Test]
        public void TestIfServiceDeniesAddingTwoSameImageInDevice()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ImageInDevice image = PostFakeImageInDevice(1,1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            List<string> errorList = _service.PostImageInDevice(image);
            int databaseSizeAfterAttempt = GetCurrentDatabaseSize();
            _service.DeleteImageInDevice(image.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(DatabaseSizeCorrect(
                    initialDatabaseSize, databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                databaseSizeAfterAttempt == databaseSizeAfterCreation &&
                errorList.Count == 1 &&
                errorList.Contains(ImageInDevicesService.ImageInDeviceAlreadyExists));
        }

        [Test]
        public void TestIfServiceDeniesUpdatingNonExistingImageInDevice()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ImageInDevice image = PostFakeImageInDevice(1,1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DeleteImageInDevice(image.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            image.ImageId = 13245;
            List<string> errorList = _service.PutImageInDevice(image.Id,image);
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize, 
                    databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                errorList.Count == 1 &&
                errorList.Contains(ImageInDevicesService.ImageInDeviceNotExist));
        }

        [Test]
        [Category("problematic")]
        public void TestIfServiceDisablesImageInDevice()
        {
            int initialDatabaseSize = GetCurrentDatabaseSize();
            ImageInDevice image = PostFakeImageInDevice(1,1);
            int databaseSizeAfterCreation = GetCurrentDatabaseSize();
            _service.DisableImageInDevice(image.Id);
            ImageInDevice imageFromDatabase = _service.GetImageInDevice(image.Id);
            _service.DeleteImageInDevice(imageFromDatabase.Id);
            int databaseSizeAfterDeletion = GetCurrentDatabaseSize();
            Assert.IsTrue(
                DatabaseSizeCorrect(initialDatabaseSize, 
                    databaseSizeAfterCreation, databaseSizeAfterDeletion) &&
                imageFromDatabase.IsActive == false);
        }

        private int GetCurrentDatabaseSize()
        {
            return _service.GetImageInDevices().Count;
        }

    }
}
