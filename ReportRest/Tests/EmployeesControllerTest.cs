﻿using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;
using System.Web.Http.Results;
using System.Web.Security;
using NUnit.Framework;
using ReportRest.Controllers;
using ReportRest.Documents;
using ReportRest.Utilities;

namespace ReportRest.Tests
{
    [TestFixture]
    public class EmployeesControllerTest : TestUtilities
    {
        private const string TestUserEmail = "zxpdegge@sharklasers.com";
       /* [Test]
        public void ControllerCreatesInvitationEventIfUserNotExist()
        {
            string email = "blabla@google.com";
            EmployeesController controller = InitializeControllerForTests();
            ConstructionSite constrcutionSite = PostFakeConstructionSite("1");
            ResponseMessageResult result = controller.AddEmployee(email, constrcutionSite.Id,
                false);
            PendingEvent pendingEventFromDabatabase = _pendingEventsService.GetPendingEvents()
                .FirstOrDefault(o => o.Email == email);
            Assert.NotNull(pendingEventFromDabatabase);
            Assert.IsFalse(pendingEventFromDabatabase.IsManager);
            Assert.AreEqual(constrcutionSite.Id, pendingEventFromDabatabase.ConstructionSiteId);
            Assert.AreEqual(email, pendingEventFromDabatabase.Email);
            _constructionSitesService.DeleteConstructionSite(constrcutionSite.Id);
            _pendingEventsService.DeletePendingEvent(pendingEventFromDabatabase.Id);
        }*/

        private EmployeesController InitializeControllerForTests()
        {
            HttpConfiguration config = new HttpConfiguration();
            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, "http://localhost/api/user/1337");
            config.Routes.MapHttpRoute("Default", "api/{controller}/{id}");
            EmployeesController controller = new EmployeesController
            {
                Request = request
            };
            controller.Request = request;
            controller.Request.Properties[HttpPropertyKeys.HttpConfigurationKey] = config;
            return controller;
        }

    }
}