﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ReportRest.Validations;

namespace ReportRest.Services
{
    public class StatusService 
    {
        public const string IdsNotMatch = "ids.not.match";
        public const string CreatorIdNotSpecified = "creator.id.not.specified";
        public const string ShortDescriptionNotSpecified = "short.description.not.specified";
        public const string DefectIdNotSpecified = "defect.id.not.specified";
        public const string StatusNotExist = "status.not.exist";
        public const string StatusAlreadyExist = "status.already.exist";
        public const string DateNotSpecified = "date.not.specified";

        private readonly ImageInDevicesService _imageInDevicesService = new ImageInDevicesService();
        private readonly ImagesService _imagesService = new ImagesService();

        public List<Status> GetMultipleStatus()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Status.ToList();
            }
        }

        public Status GetStatus(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Status.Find(id);
            }
        }

        public List<string> PutStatus(int id, Status status)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateStatus(status);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (id != status.Id)
                {
                    errorList.Add(IdsNotMatch);
                    return errorList;
                }

                _db.Entry(status).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StatusExists(id))
                    {
                        errorList.Add(StatusNotExist);
                        return errorList;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostStatus(Status status)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateStatus(status);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (GetMultipleStatus().FirstOrDefault(o => o.Compare(status)) != null)
                {
                    errorList.Add(StatusAlreadyExist);
                    return errorList;
                }

                _db.Status.Add(status);
                _db.SaveChanges();

                return null;
            }
        }

        public List<ObjectValidationError<Status>> DeleteDefectStatusesNotInPayload(
            MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Status>> allErrors = new List<ObjectValidationError<Status>>();
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                List<Status> oldStatuses =
                    StatusesFromDefectNotInPayload(myDefect);
                if (oldStatuses != null && oldStatuses.Count != 0)
                {
                    List<ObjectValidationError<Status>> deletionErrors =
                        DeleteOldStatuses(oldStatuses);
                    if (deletionErrors != null && deletionErrors.Count != 0)
                    {
                        allErrors.Union(deletionErrors);
                    }
                    else
                    {
                        DeleteItemsAssociatedWithStatus(oldStatuses);
                    }
                }
            }
            return allErrors;
        }

        public bool DeleteStatus(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                Status status = _db.Status.Find(id);
                if (status == null)
                {
                    return false;
                }

                _db.Status.Remove(status);
                _db.SaveChanges();

                return true;
            }
        }

        public List<Status> StatusesFromDefectNotInPayload(MyDefect myDefect)
        {
            List<Status> oldStatuses =
                GetAllStatusFromSameDefect(myDefect.SingleDefect.Id);
            List<Status> payloadStatuses =
                myDefect.MyMultipleStatus.Select(o => o.SingleStatus).ToList();
            List<Status> notMentionedStatuses = oldStatuses.Where(e2 => !payloadStatuses.Any(
                   e1 => e1.Compare(e2))).ToList();
            return notMentionedStatuses;
        }

        public void SetStatusIdsInPayload(MyDefect myDefect)
        {
            foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
            {
                Status matchingStatus = GetMultipleStatus().
                    FirstOrDefault(o => o.Compare(myStatus.SingleStatus));
                if (matchingStatus != null)
                {
                    myStatus.SingleStatus.Id = matchingStatus.Id;
                }
            }
        }

        public void SetForeignKeysInStatuses(MyConstructionSite myConstructionSite)
        {
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
                {
                    myStatus.SingleStatus.DefectId = myDefect.SingleDefect.Id;
                }
            }
        }

        public List<ObjectValidationError<Status>> PostNewPayloadStatuses(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Status>> postErrors = new List<ObjectValidationError<Status>>();
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                List<Status> newPayloadStatuses = GetNewPayloadStatuses(myDefect);
                if (newPayloadStatuses != null && newPayloadStatuses.Count != 0)
                {
                    List<ObjectValidationError<Status>> uploadErrors =
                        PostNewStatuses(newPayloadStatuses);
                    if (uploadErrors != null && uploadErrors.Count != 0)
                    {
                        postErrors.Union(uploadErrors);
                    }
                    else
                    {
                        SetStatusIdsInPayload(myDefect);
                        _imageInDevicesService.SetForeignKeysInImagesInDevice(
                            myDefect);
                    }
                }
            }
            return postErrors;
        }

        public List<Status> GetNewPayloadStatuses(MyDefect myDefect)
        {
            List<Status> oldStatuses = GetAllStatusFromSameDefect(myDefect.SingleDefect.Id);
            List<Status> payloadStatuses = myDefect.MyMultipleStatus.Select(o => o.SingleStatus).ToList();
            List<Status> newStatuses = GetListOfNewStatuses(oldStatuses, payloadStatuses);
            return newStatuses;
        }

        public List<ObjectValidationError<Status>> PostNewStatuses(List<Status> newStatuses)
        {
            return (from newStatus in newStatuses
                    let errorList = PostStatus(newStatus)
                    where errorList != null && errorList.Count != 0
                    select new ObjectValidationError<Status>
                    {
                        Object = newStatus,
                        ErrorList = errorList
                    }).ToList();
        }

        public void DeleteItemsAssociatedWithStatus(List<Status> statusesToDelete)
        {
            foreach (Status status in statusesToDelete)
            {
                List<ImageInDevice> imagesWithStatus =
                    _imageInDevicesService.GetAllImageInDevicesFromSameStatus(
                        status.Id);
                if (imagesWithStatus != null && imagesWithStatus.Count != 0)
                {
                    foreach (ImageInDevice image in imagesWithStatus)
                    {
                        _imageInDevicesService.DisableImageInDevice(image.Id);
                    }
                }
                List<Image> images = _imagesService.GetImagesFromStatus(
                    status.Id);
                if (images != null && images.Count != 0)
                {
                    foreach (Image image in images)
                    {
                        _imagesService.DisableImage(image.Id);
                    }
                }
            }
        }


        private List<ObjectValidationError<Status>> DeleteOldStatuses(List<Status> oldStatuses)
        {
            return (from oldStatus in oldStatuses
                    where !DisableStatus(oldStatus.Id)
                    select new ObjectValidationError<Status>
                    {
                        Object = oldStatus,
                        ErrorList = new List<string> { StatusService.StatusNotExist }
                    }).ToList();
        }

        public bool DisableStatus(int statusId)
        {
            Status status = GetStatus(statusId);
            if (status == null)
            {
                return false;
            }
            if (!status.IsActive)
            {
                return false;
            }
            status.IsActive = false;
            PutStatus(status.Id ,status);
            return true;
        }

        public List<Status> GetAllStatusFromSameDefect(int defectId)
        {
            return GetMultipleStatus().Where(o => o.DefectId == defectId && o.IsActive).ToList();
        }

        public List<Status> GetListOfNewStatuses(List<Status> oldList, List<Status> statusPayload)
        {
            return statusPayload.Where(e2 => !oldList.Any(
                   e1 => e1.Compare(e2))).ToList();
        }

        private List<string> ValidateStatus(Status status)
        {
            List<string> errorList = new List<string>();
            if (string.IsNullOrEmpty(status.CreatorId))
            {
                errorList.Add(CreatorIdNotSpecified);
            }
            if (status.DefectId == 0)
            {
                errorList.Add(DefectIdNotSpecified);
            }
            if (string.IsNullOrEmpty(status.ShortDescription))
            {
                errorList.Add(ShortDescriptionNotSpecified);
            }
            if (status.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

        private bool StatusExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Status.Count(e => e.Id == id) > 0;
            }
        }
    }
}
