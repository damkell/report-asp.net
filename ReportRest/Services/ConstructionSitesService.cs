﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ReportRest.Services
{
    public class ConstructionSitesService
    {
        public const string SiteNotExist = "site.not.exist";
        public const string IdMismatch = "ids.not.match";
        public const string CreatorIdNotSpecified = "creator.id.not.specified";
        public const string EmptyManagerId = "no.manager.id";
        public const string NoLocationId = "no.location.id";
        public const string NotAManager = "not.a.manager";
        public const string AlreadyAWorker = "already.a.worker";
        public const string AlreadyAManager = "already.a.manager";
        public const string NotAMember = "not.a.member";
        public const string SiteAlreadyExists = "site.already.exists";
        public const string DateNotSpecified = "date.not.specified";

        public List<ConstructionSite> GetConstructionSites()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.ConstructionSites.ToList();
            }
        }

        public ConstructionSite GetConstructionSite(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                ConstructionSite constructionSite = _db.ConstructionSites.Find(id);
                return constructionSite;
            }
        }

        public List<string> PutConstructionSiteAndGetErrors(int id, ConstructionSite constructionSite)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateConstructionSite(constructionSite);
                if (errorList.ToArray().Length != 0)
                {
                    return errorList;
                }
                if (id != constructionSite.Id)
                {
                    errorList.Add(IdMismatch);
                    return errorList;
                }

                _db.Entry(constructionSite).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ConstructionSiteExists(id))
                    {
                        errorList.Add(SiteNotExist);
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostConstructionSite(ConstructionSite constructionSite)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateConstructionSite(constructionSite);
                if (errorList.ToArray().Length != 0)
                {
                    return errorList;
                }
                if (GetConstructionSites().FirstOrDefault(o => o.Compare(constructionSite)) != null)
                {
                    errorList.Add(SiteAlreadyExists);
                    return errorList;
                }
                _db.ConstructionSites.Add(constructionSite);
                _db.SaveChanges();
                return null;
            }
        }

        public bool DeleteConstructionSite(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                ConstructionSite constructionSite = _db.ConstructionSites.Find(id);
                if (constructionSite == null)
                {
                    return false;
                }

                _db.ConstructionSites.Remove(constructionSite);
                _db.SaveChanges();
                return true;
            }
        }

        public List<string> DisableConstructionSite(int managerId, int constructionSiteId)
        {
            List<string> errorList = new List<string>();
            ConstructionSite constructionSite = GetConstructionSite(constructionSiteId);
            if (constructionSite == null)
            {
                errorList.Add(SiteNotExist);
                return errorList;
            }
            constructionSite.IsActive = false;
            return PutConstructionSiteAndGetErrors(constructionSite.Id, constructionSite);
        }

        private bool ConstructionSiteExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.ConstructionSites.Count(e => e.Id == id) > 0;
            }
        }

        private List<string> ValidateConstructionSite(ConstructionSite constructionSite)
        {
            List<string> errorList = new List<string>();
            if (string.IsNullOrEmpty(constructionSite.CreatorId))
            {
                errorList.Add(CreatorIdNotSpecified);
            }
            if (constructionSite.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

    }
}
