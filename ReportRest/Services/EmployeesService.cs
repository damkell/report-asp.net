﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Security;
using NUnit.Framework;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Validations;

namespace ReportRest.Services
{
    public class EmployeesService
    {
        public const string NotAManager = "not.a.manager";
        public const string UserIdNotSpecified = "user.id.not.specified";
        public const string ConstructionSiteIdNotSpecified = "construction.site.id.not.specified";
        public const string IdsNotMatch = "employee.ids.not.match";
        public const string EmployeeNotExist = "employee.not.exist";
        public const string EmployeeAlreadyExists = "employee.already.exists";
        public const string AlreadyAManager = "already.a.manager";
        public const string AlreadyAWorker = "already.a.worker";
        public const string CanNotEdit = "can.not.edit.not.a.manager";

        public List<Employee> GetEmployees()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Employees.ToList();
            }
        }

        public Employee GetEmployee(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Employees.Find(id);
            }
        }

        public List<string> PutEmployee(int id, Employee employee)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> validationErrors = ValidateEmployee(employee);
                if (validationErrors.Count != 0)
                {
                    return validationErrors;
                }

                if (id != employee.Id)
                {
                    validationErrors.Add(IdsNotMatch);
                    return validationErrors;
                }

                _db.Entry(employee).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EmployeeExists(id))
                    {
                        validationErrors.Add(EmployeeNotExist);
                        return validationErrors;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<Employee> GetListOfNewEmployees(List<Employee> payload, List<Employee> oldEmployees)
        {
            return payload.Where(e2 => !oldEmployees.Any(
                   e1 => e1.Compare(e2))).ToList();
        }

        public List<string> PostEmployee(Employee employee)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errors = ValidateEmployee(employee);
                if (errors.Count != 0)
                {
                    return errors;
                }
                if (GetEmployees().FirstOrDefault(o => o.Compare(employee)) != null)
                {
                    errors.Add(EmployeeAlreadyExists);
                    return errors;
                }

                _db.Employees.Add(employee);
                _db.SaveChanges();

                return null;
            }
        }

        public bool DeleteEmployee(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                Employee employee = _db.Employees.Find(id);
                if (employee == null)
                {
                    return false;
                }

                _db.Employees.Remove(employee);
                _db.SaveChanges();
                return true;
            }
        }

        public List<Employee> GetAllEmployeesFromConstructionSite(int constructionSiteId)
        {
            return GetEmployees().Where(o => o.ConstructionSiteId == constructionSiteId && o.IsActive).ToList();
        } 

        public string ChangeEmployeeStatusToManager(int id)
        {
            Employee employee = GetEmployee(id);
            if (employee == null)
            {
                return EmployeeNotExist;
            }
            if (employee.IsManager)
            {
                return AlreadyAManager;
            }
            employee.IsManager = true;
            PutEmployee(id, employee);
            return null;
        }

        public string ChangeEmployeeStatusToWorker(int id)
        {
            Employee employee = GetEmployee(id);
            if (employee == null)
            {
                return EmployeeNotExist;
            }
            if (!employee.IsManager)
            {
                return AlreadyAWorker;
            }
            employee.IsManager = false;
            PutEmployee(id, employee);
            return null;
        }

        public bool IsEmployeeAManager(int id)
        {
            Employee maybeEmployee = GetEmployee(id);
            return maybeEmployee != null && maybeEmployee.IsManager;
        }

        private bool EmployeeExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Employees.Count(e => e.Id == id) > 0;
            }

        }

        public List<ObjectValidationError<Employee>> PostNewPayloadEmployees(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Employee>> errors = new List<ObjectValidationError<Employee>>();
            List<Employee> payloadEmployees = GetNewPayloadEmployees(myConstructionSite);
            if (payloadEmployees != null && payloadEmployees.Count != 0)
            {
                errors.AddRange(from employee in payloadEmployees
                                let postErrors = PostEmployee(employee)
                                where postErrors != null && postErrors.Count != 0
                                select new ObjectValidationError<Employee>
                                {
                                    Object = employee,
                                    ErrorList = postErrors
                                });
            }
            return errors;
        }

        public List<ObjectValidationError<Employee>> DeleteOldEmployeesNotIncludedInPayload(
            MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Employee>> errors = new List<ObjectValidationError<Employee>>();
            List<Employee> oldEmployees = GetOldEmployeesNotMentionedInPayload(myConstructionSite);
            if (oldEmployees != null && oldEmployees.Count != 0)
            {
                errors.AddRange(from employee in oldEmployees
                                where !DisableEmployee(employee.Id)
                                select new ObjectValidationError<Employee>
                                {
                                    Object = employee,
                                    ErrorList = new List<string> { EmployeesService.EmployeeNotExist }
                                });
            }
            return errors;
        }

        public List<Employee> GetNewPayloadEmployees(MyConstructionSite myConstructionSite)
        {
            List<Employee> oldEmployees =
                GetAllEmployeesFromConstructionSite(myConstructionSite.SingleConstructionSite.Id);
            List<Employee> payloadEmployees = myConstructionSite.Employees;
            return GetListOfNewEmployees(payloadEmployees, oldEmployees);
        }

        public List<Employee> GetOldEmployeesNotMentionedInPayload(MyConstructionSite myConstructionSite)
        {
            List<Employee> oldEmployees =
                GetAllEmployeesFromConstructionSite(myConstructionSite.SingleConstructionSite.Id);
            List<Employee> payloadEmployees = myConstructionSite.Employees;
            return oldEmployees.Where(e2 => !payloadEmployees.Any(
                   e1 => e1.Compare(e2))).ToList();
        }


        public void SetForeignKeysInEmployees(MyConstructionSite myConstructionSite)
        {
            foreach (Employee employee in myConstructionSite.Employees)
            {
                employee.ConstructionSiteId = myConstructionSite.SingleConstructionSite.Id;
            }
        }


        public void UpdateEmployees(MyConstructionSite myConstructionSite)
        {
            foreach (Employee employee in myConstructionSite.Employees)
            {
                if (employee.Id != 0)
                {
                    Employee matchingEmployee = GetEmployee(employee.Id);
                    if (matchingEmployee.IsManager && !employee.IsManager)
                    {
                        ChangeEmployeeStatusToWorker(employee.Id);
                    }
                    if (!matchingEmployee.IsManager && employee.IsManager)
                    {
                        ChangeEmployeeStatusToManager(employee.Id);
                    }
                }
            }
        }

        public bool DisableEmployee(int id)
        {
            Employee employee = GetEmployee(id);
            if (employee == null)
            {
                return false;
            }
            employee.IsActive = false;
            List<string> errors = PutEmployee(id, employee);
            if (errors != null && errors.Count != 0)
            {
                return false;
            }
            return true;
        }

        private List<string> ValidateEmployee(Employee employee)
        {
            List<string> validationErrors = new List<string>();
            if (string.IsNullOrEmpty(employee.UserId))
            {
                validationErrors.Add(UserIdNotSpecified);
            }
            if (employee.ConstructionSiteId == 0)
            {
                validationErrors.Add(ConstructionSiteIdNotSpecified);
            }
            return validationErrors;
        } 

    }
}