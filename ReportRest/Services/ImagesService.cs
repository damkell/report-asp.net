﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ReportRest.Services
{
    public class ImagesService
    {
        public const string ImageNotExist = "image.not.exist";
        public const string ImageNotSpecified = "image.not.specified";
        public const string ImageAlreadyExists = "image.already.exists";
        public const string IdsMismatch = "ids.mismatch";
        public const string CreatorIdNotSpecified = "creator.id.not.specified";
        public const string ImageInBytesNotSpecified = "image.in.bytes.not.specified";
        public const string StatusIdNotSpecified = "status.id.not.specified";
        public const string DateNotSpecified = "date.not.specified";

        public List<Image> GetImages()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Images.ToList();
            }
        }

        public Image GetImage(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Images.Find(id);
            }
        }

        public List<Image> GetImagesFromStatus(int statusId)
        {
            return GetImages().Where(o => o.StatusId == statusId).ToList();
        } 

        public List<string> PutImage(int id, Image image)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateImage(image);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (id != image.Id)
                {
                    errorList.Add(IdsMismatch);
                    return errorList;
                }

                _db.Entry(image).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImageExists(id))
                    {
                        errorList.Add(ImageNotExist);
                        return errorList;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostImage(Image image)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateImage(image);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (GetImages().FirstOrDefault(o => o.Compare(image)) != null)
                {
                    errorList.Add(ImageAlreadyExists);
                    return errorList;
                }
                _db.Images.Add(image);
                _db.SaveChanges();

                return null;
            }
        }

        public bool DeleteImage(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                Image image = _db.Images.Find(id);
                if (image == null)
                {
                    return false;
                }

                _db.Images.Remove(image);
                _db.SaveChanges();

                return true;
            }
        }

        public bool DisableImage(int id)
        {
            Image image = GetImage(id);
            if (image == null)
            {
                return false;
            }
            if (!image.IsActive)
            {
                return false;
            }
            image.IsActive = false;
            PutImage(image.Id, image);
            return true;
        }

        private List<string> ValidateImage(Image image)
        {
            List<string> errorList = new List<string>();
            if (image == null)
            {
                errorList.Add(ImageNotSpecified);
                return errorList;
            }
            if (image.PhotoInBytes == null)
            {
                errorList.Add(ImageInBytesNotSpecified);
            }
            if (image.StatusId == 0)
            {
                errorList.Add(StatusIdNotSpecified);
            }
            if (image.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

        private bool ImageExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Images.Count(e => e.Id == id) > 0;
            }
        }
    }
}
