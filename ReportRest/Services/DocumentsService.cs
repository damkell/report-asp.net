﻿
namespace ReportRest.Services
{
    public abstract class DocumentsService
    {
        protected readonly ConstructionSitesService _constructionSitesService = new ConstructionSitesService();
        protected readonly LocationsService _locationsService = new LocationsService();
        protected readonly DefectsService _defectsService = new DefectsService();
        protected readonly ImageInDevicesService _imageInDevicesService = new ImageInDevicesService();
        protected readonly StatusService _statusService = new StatusService();
        protected readonly EmployeesService _employeesService = new EmployeesService();
        protected readonly ImagesService _imagesService = new ImagesService();
        protected readonly PendingEventsService _pendingEventsService = new PendingEventsService();
    }
}