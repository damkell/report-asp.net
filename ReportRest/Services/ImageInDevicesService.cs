﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ReportRest.Validations;

namespace ReportRest.Services
{
    public class ImageInDevicesService
    {
        public const string ImageInDeviceNotExist = "image.in.device.not.exist";
        public const string ImageInDeviceAlreadyExists = "image.in.device.already.exists";
        public const string IdsMismatch = "ids.do.not.match";
        public const string DeviceIdNotSpecified = "device.id.not.specified";
        public const string ImageLocationNotSpecified = "image.location.not.specified";
        public const string DateNotSpecified = "date.not.specified";
        public const string ImageInDeviceNotSpecified = "image.in.device.not.specified";

        public List<ImageInDevice> GetImageInDevices()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.ImageInDevices.ToList();
            }
        }

        public ImageInDevice GetImageInDevice(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.ImageInDevices.Find(id);
            }
        }

        public List<string> PutImageInDevice(int id, ImageInDevice imageInDevice)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateImageInDevice(imageInDevice);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (id != imageInDevice.Id)
                {
                    errorList.Add(IdsMismatch);
                    return errorList;
                }

                _db.Entry(imageInDevice).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ImageInDeviceExists(id))
                    {
                        errorList.Add(ImageInDeviceNotExist);
                        return errorList;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostImageInDevice(ImageInDevice imageInDevice)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorsList = ValidateImageInDevice(imageInDevice);
                if (errorsList.Count != 0)
                {
                    return errorsList;
                }
                if (GetImageInDevices().FirstOrDefault(o => o.Compare(imageInDevice)) != null)
                {
                    errorsList.Add(ImageInDeviceAlreadyExists);
                    return errorsList;
                }

                _db.ImageInDevices.Add(imageInDevice);
                _db.SaveChanges();

                return null;
            }
        }

        public void DeleteImageInDevicesFromSameStatus(int statusId)
        {
            List<ImageInDevice> imagesToDelete = GetAllImageInDevicesFromSameStatus(statusId);
            foreach (ImageInDevice imageInDevice in imagesToDelete)
            {
                DeleteImageInDevice(imageInDevice.Id);
            }
        }

        public List<ImageInDevice> GetAllImageInDevicesFromSameStatus(int statusId)
        {
            return GetImageInDevices().Where(o => o.StatusId == statusId).ToList();
        }

        public List<ImageInDevice> GetListOfNewImageInDevices(List<ImageInDevice> oldList, List<ImageInDevice> imageInDevicesPayload)
        {
            return imageInDevicesPayload.Where(e2 => !oldList.Any(
                   e1 => e1.Compare(e2))).ToList();
        }

        public bool DeleteImageInDevice(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                ImageInDevice imageInDevice = _db.ImageInDevices.Find(id);
                if (imageInDevice == null)
                {
                    return false;
                }

                _db.ImageInDevices.Remove(imageInDevice);
                _db.SaveChanges();

                return true;
            }
        }

        public bool DisableImageInDevice(int id)
        {
            ImageInDevice imageInDevice = GetImageInDevice(id);
            if (imageInDevice == null || !imageInDevice.IsActive)
            {
                return false;
            }
            imageInDevice.IsActive = false;
            PutImageInDevice(id, imageInDevice);
            return true;
        }

        public List<ObjectValidationError<ImageInDevice>> DeleteOldImageInDevices(
           List<ImageInDevice> imageInDevices)
        {
            return (from imageInDevice in imageInDevices
                    where !DisableImageInDevice(imageInDevice.Id)
                    select new ObjectValidationError<ImageInDevice>
                    {
                        Object = imageInDevice,
                        ErrorList = new List<string> { ImageInDevicesService.ImageInDeviceNotExist }
                    }).ToList();
        }

        public List<ObjectValidationError<ImageInDevice>> PostNewPayloadImageInDevices(
            MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<ImageInDevice>> postErrors =
                new List<ObjectValidationError<ImageInDevice>>();
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
                {
                    List<ImageInDevice> newImageInDevices = GetNewPayloadImageInDevices(myStatus);
                    if (newImageInDevices != null && newImageInDevices.Count != 0)
                    {
                        List<ObjectValidationError<ImageInDevice>> errors =
                            PostNewImageInDevices(newImageInDevices);
                        if (errors != null && errors.Count != 0)
                        {
                            postErrors.Union(errors);
                        }
                    }
                }
            }
            return postErrors;
        }

        private List<ObjectValidationError<ImageInDevice>> PostNewImageInDevices(
            List<ImageInDevice> newImageInDevices)
        {
            return (from newImage in newImageInDevices
                    let errorList = PostImageInDevice(newImage)
                    where errorList != null && errorList.Count != 0
                    select new ObjectValidationError<ImageInDevice>
                    {
                        Object = newImage,
                        ErrorList = errorList
                    }).ToList();
        }

        public List<ImageInDevice> GetOldImageInDevicesNotIncludedInPayload(MyStatus myStatus)
        {
            if (myStatus.ImagesInDevice == null || myStatus.ImagesInDevice.Count == 0)
            {
                return null;
            }
            List<ImageInDevice> oldImageInDevices =
                GetAllImageInDevicesFromSameStatus(myStatus.SingleStatus.Id);
            List<ImageInDevice> payloadDefects = myStatus.ImagesInDevice;
            List<ImageInDevice> notMentionedOldImageInDevices = oldImageInDevices.Where(e2 => !payloadDefects.Any(
                   e1 => e1.Compare(e2))).ToList();
            return notMentionedOldImageInDevices;
        }



        public List<ImageInDevice> GetNewPayloadImageInDevices(MyStatus myStatus)
        {
            if (myStatus.ImagesInDevice == null || myStatus.ImagesInDevice.Count == 0)
            {
                return null;
            }
            List<ImageInDevice> oldImageInDevices = 
                GetAllImageInDevicesFromSameStatus(myStatus.SingleStatus.Id);
            List<ImageInDevice> payloadImageInDevices = myStatus.ImagesInDevice;
            List<ImageInDevice> newImageInDevices = 
                GetListOfNewImageInDevices(
                oldImageInDevices, payloadImageInDevices);
            return newImageInDevices;
        }

        public List<ObjectValidationError<ImageInDevice>> DeleteOldImageInDevices(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<ImageInDevice>> deletionErrors = new List<ObjectValidationError<ImageInDevice>>();
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
                {
                    List<ImageInDevice> oldImageInDevices =
                        GetOldImageInDevicesNotIncludedInPayload(myStatus);
                    if (oldImageInDevices != null && oldImageInDevices.Count != 0)
                    {
                        List<ObjectValidationError<ImageInDevice>> errors =
                            DeleteOldImageInDevices(oldImageInDevices);
                        if (errors != null && errors.Count != 0)
                        {
                            deletionErrors.Union(errors);
                        }
                    }
                }
            }
            return deletionErrors;
        }

        public List<ImageInDevice> GetAllImageInDevicesFromStatusAndDevice(int statusId, int deviceId)
        {
            return GetAllImageInDevicesFromSameStatus(statusId).Where(o => o.StatusId == statusId && o.IsActive).ToList();
        } 

        public void SetForeignKeysInImagesInDevice(MyDefect myDefect)
        {
            foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
            {
                if (myStatus.ImagesInDevice != null)
                {
                    foreach (ImageInDevice imageInDevice in myStatus.ImagesInDevice)
                    {
                        imageInDevice.StatusId = myStatus.SingleStatus.Id;
                    }
                }
            }
        }

        private List<string> ValidateImageInDevice(ImageInDevice imageInDevice)
        {
            List<string> errorList = new List<string>();
            if (imageInDevice == null)
            {
                errorList.Add(ImageInDeviceNotSpecified);
            }
            if (imageInDevice != null && string.IsNullOrEmpty(imageInDevice.ImageLocation))
            {
                errorList.Add(ImageLocationNotSpecified);
            }
            if (imageInDevice != null && imageInDevice.DeviceId == 0)
            {
                errorList.Add(DeviceIdNotSpecified);
            }
            if (imageInDevice != null && imageInDevice.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

        private bool ImageInDeviceExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.ImageInDevices.Count(e => e.Id == id) > 0;
            }
        }
    }
}
