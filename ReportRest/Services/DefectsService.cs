﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ReportRest.Validations;

namespace ReportRest.Services
{
    public class DefectsService
    {
        public const string DefectNotExist = "defect.not.exist";
        public const string DefectAlreadyExist = "defect.already.exist";
        public const string CreatorIdNotSpecified = "creator.id.not.specified";
        public const string ConstructionSiteIdNotSpecified = "construction.site.id.not.specified";
        public const string IdsMismatch = "ids.mismatch";
        public const string DateNotSpecified = "date.not.specified";
        private readonly StatusService _statusService = new StatusService();
        public List<Defect> GetDefects()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Defects.ToList();
            }
        }

        public Defect GetDefect(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Defects.Find(id);
            }
        }

        public List<string> PutDefect(int id, Defect defect)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateDefect(defect);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (id != defect.Id)
                {
                    errorList.Add(IdsMismatch);
                    return errorList;
                }
                _db.Entry(defect).State = EntityState.Modified;
                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!DefectExists(id))
                    {
                        errorList.Add(DefectNotExist);
                        return errorList;
                    }
                    else
                    {
                        throw;
                    }
                }
                return null;
            }
        }

        public List<string> PostDefect(Defect defect)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateDefect(defect);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                Defect maybeDefect = GetDefects().FirstOrDefault(o => o.Compare(defect));
                if (maybeDefect != null)
                {
                    errorList.Add(DefectAlreadyExist);
                    return errorList;
                }
                _db.Defects.Add(defect);
                _db.SaveChanges();
                return null;
            }
        }

        public bool DeleteDefect(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                Defect defect = _db.Defects.Find(id);
                if (defect == null)
                {
                    return false;
                }

                _db.Defects.Remove(defect);
                _db.SaveChanges();
                return true;
            }
        }

        public bool DisableDefect(int id)
        {
            Defect defect = GetDefect(id);
            if (defect == null || !defect.IsActive)
            {
                return false;
            }
            defect.IsActive = false;
            PutDefect(defect.Id, defect);
            return true;
        }

        public bool MarkDefectAsFixed(int id)
        {
            Defect defect = GetDefect(id);
            if (defect == null)
            {
                return false;
            }
            if (defect.IsFixed)
            {
                return false;
            }
            defect.IsFixed = true;
            PutDefect(id, defect);
            return true;
        }

        public void DeleteAllDefectsFromConstructionSite(int constructionSite)
        {
            List<Defect> defectsToDelete = GetAllDefectsFromConstructionSite(constructionSite);
            foreach (Defect defect in defectsToDelete)
            {
                DeleteDefect(defect.Id);
            }
        }

        public List<ObjectValidationError<Defect>> DeleteOldDefectsNotIncludedInPayload(
            MyConstructionSite myConstructionSite)
        {
            List<Defect> oldDefects = GetOldDefectsNotIncludedInPayload(myConstructionSite);
            if (oldDefects != null && oldDefects.Count != 0)
            {
                List<ObjectValidationError<Defect>> deletionErrors =
                    DeleteOldDefects(oldDefects);
                if (deletionErrors != null && deletionErrors.Count != 0)
                {
                    return deletionErrors;
                }
                DeleteItemsAssociatedWithDefect(oldDefects);
            }
            return null;
        }

        public List<ObjectValidationError<Defect>> DeleteOldDefects(List<Defect> oldDefects)
        {
            List<ObjectValidationError<Defect>> errors = new List<ObjectValidationError<Defect>>();
            foreach (Defect oldDefect in oldDefects)
            {
                oldDefect.IsActive = false;
                List<string> putErrors = PutDefect(oldDefect.Id, oldDefect);
                if (putErrors != null && putErrors.Count != 0)
                    errors.Add(new ObjectValidationError<Defect>
                    {
                        Object = oldDefect,
                        ErrorList = putErrors
                    });
            }
            return errors;
        }

        private void DeleteItemsAssociatedWithDefect(List<Defect> defects)
        {
            foreach (Defect defect in defects)
            {
                List<Status> statusesFromSameDefect =
                _statusService.GetAllStatusFromSameDefect(defect.Id);
                if (statusesFromSameDefect != null && statusesFromSameDefect.Count != 0)
                {
                    foreach (Status status in statusesFromSameDefect)
                    {
                        _statusService.DisableStatus(status.Id);
                    }
                    _statusService.
                        DeleteItemsAssociatedWithStatus(statusesFromSameDefect);
                }

            }
        }

        public List<ObjectValidationError<Defect>> UpdatePayloadDefects(MyConstructionSite myConstructionSite)
        {
            List<Defect> defectsToUpdate = myConstructionSite.MyDefects.Select(o => o.SingleDefect).ToList();
            List<ObjectValidationError<Defect>> updationErrors = UpdateDefects(defectsToUpdate);
            if (updationErrors != null && updationErrors.Count != 0)
            {
                return updationErrors;
            }
            return null;
        }

        public List<ObjectValidationError<Defect>> UpdateDefects(List<Defect> defectsToUpdate)
        {
            return (from defect in defectsToUpdate
                    where defect.IsFixed
                    let errorList = PutDefect(defect.Id, defect)
                    where errorList != null && errorList.Count != 0
                    select new ObjectValidationError<Defect>
                    {
                        Object = defect,
                        ErrorList = errorList
                    }).ToList();
        }

        public List<ObjectValidationError<Defect>> PostNewDefects(List<Defect> newDefects)
        {
            return (from newDefect in newDefects
                    let errorList = PostDefect(newDefect)
                    where errorList != null && errorList.Count != 0
                    select new ObjectValidationError<Defect>
                    {
                        Object = newDefect,
                        ErrorList = errorList
                    }).ToList();
        }

        public void SetForeignKeysInDefects(MyConstructionSite myConstructionSite)
        {
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                myDefect.SingleDefect.ConstructionSiteId = myConstructionSite.SingleConstructionSite.Id;
            }
        }

        public List<Defect> GetOldDefectsNotIncludedInPayload(MyConstructionSite myConstructionSite)
        {
            List<Defect> oldDefects =
                GetAllDefectsFromConstructionSite(myConstructionSite.SingleConstructionSite.Id);
            List<Defect> payloadDefects = myConstructionSite.MyDefects.Select(o => o.SingleDefect).ToList();
            List<Defect> notMentionedOldDefects = oldDefects.Where(e2 => !payloadDefects.Any(
                   e1 => e1.Compare(e2))).ToList();
            return notMentionedOldDefects;
        }

        public List<Defect> GetNewPayloadDefects(MyConstructionSite myConstructionSite)
        {
            List<Defect> oldDefects = GetAllDefectsFromConstructionSite(myConstructionSite.SingleConstructionSite.Id);
            List<Defect> payloadDefects = myConstructionSite.MyDefects.Select(myDefect => myDefect.SingleDefect).ToList();
            List<Defect> newDefects = GetListOfNewDefects(oldDefects, payloadDefects);
            return newDefects;
        }

        public List<ObjectValidationError<Defect>> PostNewPayloadDefects(MyConstructionSite myConstructionSite)
        {
            List<Defect> newDefects = GetNewPayloadDefects(myConstructionSite);
            if (newDefects != null && newDefects.Count != 0)
            {
                List<ObjectValidationError<Defect>> error = PostNewDefects(newDefects);
                if (error != null && error.Count != 0)
                {
                    return error;
                }
                SetDefectIdsInPayload(myConstructionSite);
                _statusService.SetForeignKeysInStatuses(myConstructionSite);
            }
            return null;
        }

        private void SetDefectIdsInPayload(MyConstructionSite myConstructionSite)
        {
            foreach (MyDefect myDefect in myConstructionSite.MyDefects)
            {
                Defect matchingDefect = GetDefects().
                    FirstOrDefault(o => o.Compare(myDefect.SingleDefect));
                if (matchingDefect != null)
                {
                    myDefect.SingleDefect.Id = matchingDefect.Id;
                    myDefect.SingleDefect.Id = matchingDefect.Id;
                }
            }
        }

        public List<Defect> GetAllDefectsFromConstructionSite(int constructionSiteId)
        {
            return GetDefects().Where(o => o.ConstructionSiteId == constructionSiteId && o.IsActive).ToList();
        }

        public List<Defect> GetListOfNewDefects(List<Defect> oldList, List<Defect> defectsPayload)
        {
            return defectsPayload.Where(e2 => !oldList.Any(
                   e1 => e1.Compare(e2))).ToList();
        }

        private List<string> ValidateDefect(Defect defect)
        {
            List<string> errorList = new List<string>();
            if (string.IsNullOrEmpty(defect.CreatorId))
            {
                errorList.Add(CreatorIdNotSpecified);
            }
            if (defect.ConstructionSiteId == 0)
            {
                errorList.Add(ConstructionSiteIdNotSpecified);
            }
            if (defect.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

        private bool DefectExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Defects.Count(e => e.Id == id) > 0;
            }
        }

    }
}
