﻿using System.Net;
using System.Net.Mail;

namespace ReportRest.Services
{
    class EmailService
    {

        private const string HostAddress = "smtp.gmail.com";
        private const string SenderAddress = "mantttttas@gmail.com";
        private const int ServerPort = 587;

        public static void SendEmail(string address, string message)
        {
            using (MailMessage mailMessage = new MailMessage())
            {
                mailMessage.To.Add(new MailAddress(address));
                mailMessage.From = new MailAddress(SenderAddress);
                mailMessage.Subject = message;
                SmtpClient smtpClient = new SmtpClient
                {
                    Host = HostAddress,
                    EnableSsl = true,
                    UseDefaultCredentials = true,
                    Credentials = new NetworkCredential(SenderAddress, "logitech19960705"),
                    Port = ServerPort
                };
                smtpClient.Send(mailMessage);
            }       
        }

    }
}
