﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using ReportRest.Models;
using ReportRest.Documents;

namespace ReportRest.Services
{
    public class PendingEventsService 
    {
        public const string InviterIdNotSpecified = "inviter.id.not.specified";
        public const string GroupIdNotSpecified = "group.id.not.specified";
        public const string HashNotSpecified = "hash.not.specified";
        public const string IdsNotMatch = "id.not.match";
        public const string PendingEventNotExist = "pending.event.not.exist";
        public const string PendingEventAlreadyExist = "pending.event.already.exist";
        public const string DateNotSpecified = "date.not.specified";

        public List<PendingEvent> GetPendingEvents()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.PendingEvents.ToList();
            }
        }

        public List<PendingEvent> GetPendingEventsOfUser(string userId)
        {
            return GetPendingEvents().Where(o => o.UserId == userId && o.IsActive).ToList();
        } 

        public List<string> CreateRegistrationEvent(string email)
        {
            PendingEvent pendingEvent = new PendingEvent
            {
                CreatedAt = DateTime.Now,
                Email = email,
                EventType = PendingEvent.Event.Registration,
                Hash = Guid.NewGuid().ToString("n").Substring(0, 32),
                IsActive = true
            };
            List<string> errorList = PostPendingEventAndGetErrors(pendingEvent);
            if (errorList != null)
            {
                return errorList;
            }
            EmailService.SendEmail(email, "registration hash: " + pendingEvent.Hash);
            return null;
        }

        public List<string> CreateInvitationEvent(string email, int constructionSiteId, string inviterId, bool isManager)
        {
            PendingEvent pendingEvent = new PendingEvent
            {
                ConstructionSiteId = constructionSiteId,
                CreatedAt = DateTime.Now,
                Email = email,
                EventType = PendingEvent.Event.Invitation,
                IsActive = true,
                InviterId = inviterId,
                Hash = Guid.NewGuid().ToString("n").Substring(0, 32),
                IsManager = isManager
            };
            List<string> errorList = PostPendingEventAndGetErrors(pendingEvent);
            if (errorList != null)
            {
                return errorList;
            }
            EmailService.SendEmail(email,"invitation: " + pendingEvent.Hash);
            return null;
        }

        public List<string> CreateAdditionEvent(string inviterId, int constructionSiteId, bool isManager, string userId)
        {
            PendingEvent pendingEvent = new PendingEvent
            {
                CreatedAt = DateTime.Now,
                ConstructionSiteId = constructionSiteId,
                EventType = PendingEvent.Event.Addition,
                InviterId = inviterId,
                IsManager = isManager,
                IsActive = true,
            };
            List<string> errorList = PostPendingEventAndGetErrors(pendingEvent);
            return errorList ?? null;
        }

        public PendingEvent GetPendingEvent(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.PendingEvents.Find(id);
            }
        }

        public List<string> PutPendingEventAndGetErrors(int id, PendingEvent pendingEvent)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> validationErrors = ValidatePendingEvent(pendingEvent);
                if (validationErrors.Count != 0)
                {
                    return validationErrors;
                }
                if (id != pendingEvent.Id)
                {
                    validationErrors.Add(IdsNotMatch);
                    return validationErrors;
                }

                _db.Entry(pendingEvent).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PendingEventExists(id))
                    {
                        validationErrors.Add(PendingEventNotExist);
                        return validationErrors;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostPendingEventAndGetErrors(PendingEvent pendingEvent)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> validationErrors = ValidatePendingEvent(pendingEvent);
                if (validationErrors.Count != 0)
                {
                    return validationErrors;
                }
                if (PendingEventExists(pendingEvent.Id))
                {
                    validationErrors.Add(PendingEventAlreadyExist);
                    return validationErrors;
                }
                if (GetPendingEvents().FirstOrDefault(o => o.Compare(pendingEvent)) != null)
                {
                    validationErrors.Add(PendingEventAlreadyExist);
                    return validationErrors;
                }
                _db.PendingEvents.Add(pendingEvent);
                _db.SaveChanges();
                return null;
            }
        }

        public bool DeletePendingEvent(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                PendingEvent pendingEvent = _db.PendingEvents.Find(id);
                if (pendingEvent == null)
                {
                    return false;
                }

                _db.PendingEvents.Remove(pendingEvent);
                _db.SaveChanges();

                return true;
            }
        }

        public List<string> DisablePendingEvent(PendingEvent pendingEvent)
        {
            pendingEvent.IsActive = false;
            return PutPendingEventAndGetErrors(pendingEvent.Id, pendingEvent);
        }

        private bool PendingEventExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.PendingEvents.Count(e => e.Id == id) > 0;
            }
        }

        private List<string> ValidatePendingEvent(PendingEvent pendingEvent)
        {
            List<string> validationErrors = new List<string>();
            if (string.IsNullOrEmpty(pendingEvent.InviterId) && pendingEvent.EventType != PendingEvent.Event.Registration)
            {
                validationErrors.Add(InviterIdNotSpecified);
            }
            if ((pendingEvent.EventType == PendingEvent.Event.Addition|| pendingEvent.EventType == PendingEvent.Event.Invitation)
                && pendingEvent.ConstructionSiteId == 0) {
                validationErrors.Add(GroupIdNotSpecified);
            }
            if ((pendingEvent.EventType == PendingEvent.Event.Registration|| pendingEvent.EventType == PendingEvent.Event.Invitation)
                && string.IsNullOrEmpty(pendingEvent.Hash)) {
                    validationErrors.Add(HashNotSpecified);
            }
            if (pendingEvent.CreatedAt == DateTime.MinValue)
            {
                validationErrors.Add(DateNotSpecified);
            }
            return validationErrors;
        }

    }

}