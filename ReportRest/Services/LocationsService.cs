﻿using ReportRest.Documents;
using ReportRest.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace ReportRest.Services
{
    public class LocationsService
    {
        public static string CreatorIdNotSpecified = "creator.id.not.specified";
        public static string ConstructionSiteIdNotSpecified = "construction.site.id.not.specified";
        public static string DescriptionNotSpecified = "description.not.specified";
        public static string LocationAlreadyExists = "location.already.exists";
        public static string LocationNotExist = "location.not.exist";
        public static string IdsMismatch = "id.mismatch";
        public static string DateNotSpecified = "date.not.specified";

        public List<Location> GetLocations()
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Locations.ToList();
            }
        }

        public Location GetLocation(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Locations.Find(id);
            }
        }

        public List<string> PutLocation(int id, Location location)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateLocation(location);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (id != location.Id)
                {
                    errorList.Add(IdsMismatch);
                    return errorList;
                }
                _db.Entry(location).State = EntityState.Modified;

                try
                {
                    _db.SaveChanges();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!LocationExists(id))
                    {
                        errorList.Add(LocationNotExist);
                        return errorList;
                    }
                    else
                    {
                        throw;
                    }
                }

                return null;
            }
        }

        public List<string> PostLocation(Location location)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                List<string> errorList = ValidateLocation(location);
                if (errorList.Count != 0)
                {
                    return errorList;
                }
                if (GetLocations().FirstOrDefault(o => o.Compare(location)) != null)
                {
                    errorList.Add(LocationAlreadyExists);
                    return errorList;
                }

                _db.Locations.Add(location);
                _db.SaveChanges();

                return null;
            }
        }

        public bool DisableLocation(int locationId)
        {
            if (!LocationExists(locationId))
            {
                return false;
            }
            Location location = GetLocation(locationId);
            if(!location.IsActive){
                return false;
            }
            PutLocation(locationId, location);
            return true;
        }

        public bool DeleteLocation(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                Location location = _db.Locations.Find(id);
                if (location == null)
                {
                    return false;
                }

                _db.Locations.Remove(location);
                _db.SaveChanges();

                return true;
            }
        }

        private bool LocationExists(int id)
        {
            using (ApplicationDbContext _db = new ApplicationDbContext())
            {
                return _db.Locations.Count(e => e.Id == id) > 0;
            }
        }

        public Location GetLocationFromConstructionSite(int constructionSiteId)
        {
            return GetLocations().FirstOrDefault(o => o.ConstructionSiteId == constructionSiteId && o.IsActive);
        }

        private List<string> ValidateLocation(Location location)
        {
            List<string> errorList = new List<string>();
            if (location.ConstructionSiteId == 0){
                errorList.Add(ConstructionSiteIdNotSpecified);
            }
            if (string.IsNullOrEmpty(location.CreatorId))
            {
                errorList.Add(CreatorIdNotSpecified);
            }
            if (string.IsNullOrEmpty(location.Description))
            {
                errorList.Add(DescriptionNotSpecified);
            }
            if (location.CreatedAt == DateTime.MinValue)
            {
                errorList.Add(DateNotSpecified);
            }
            return errorList;
        }

    }
}
