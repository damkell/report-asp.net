﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using ReportRest.Documents;
using ReportRest.Validations;
using ReportRest.Models;

namespace ReportRest.Services
{
    public class MyConstructionSiteService : DocumentsService
    {
        public ObjectValidationError<Location> PostLocationAndIfItIsNotValidGetError(Location location)
        {
            ObjectValidationError<Location> validationError = new ObjectValidationError<Location>
            {
                Object = location
            };
            ConstructionSite associatedSite = _constructionSitesService.
                GetConstructionSite(location.ConstructionSiteId);
            if (associatedSite == null)
            {
                validationError.ErrorList = new List<string>
                {
                    ConstructionSitesService.SiteNotExist
                };
                return validationError;
            }
            List<string> locationUploadErrors = _locationsService.PostLocation(location);
            if (locationUploadErrors != null)
            {
                validationError.ErrorList = locationUploadErrors;
                return validationError;
            }
            return null;
        }

        public List<string> PostCreatorAsFirstConstructionSiteEmployee(MyConstructionSite myConstructionSite)
        {
            Employee employee = new Employee
            {
                ConstructionSiteId = myConstructionSite.SingleConstructionSite.Id,
                CreatorId = myConstructionSite.SingleConstructionSite.CreatorId,
                CreatedAt = DateTime.Now,
                IsActive = true,
                UserId = myConstructionSite.SingleConstructionSite.CreatorId,
                IsManager = true
            };
            return _employeesService.PostEmployee(employee);
        }

        public ObjectValidationError<ConstructionSite> PostConstructionSiteAndIfItIsNotValidGetError(ConstructionSite constructionSite)
        {
            List<string> constructionSiteUploadErrors =
                _constructionSitesService.PostConstructionSite(constructionSite);
            if (constructionSiteUploadErrors != null)
            {
                return new ObjectValidationError<ConstructionSite>
                {
                    Object = constructionSite,
                    ErrorList = constructionSiteUploadErrors
                };
            }
            return null;
        }

        public void DeleteConstructionSiteFromModelWithoudId(ConstructionSite site)
        {
            ConstructionSite siteFromDatabase = _constructionSitesService.GetConstructionSites().
                FirstOrDefault(o => o.Compare(site));
            if (siteFromDatabase != null)
            {
                _constructionSitesService.DeleteConstructionSite(siteFromDatabase.Id);
            }
        }

        public ObjectValidationError<Location> UpdateLocation(MyConstructionSite myConstructionSite)
        {
            Location matchingLocation = _locationsService.GetLocations().
                FirstOrDefault(o => o.Id == myConstructionSite.SingleLocation.Id);
            if (matchingLocation != null &&
                !myConstructionSite.SingleLocation.Compare(matchingLocation))
            {
                List<string> errorList = _locationsService.PutLocation(
                    matchingLocation.Id, matchingLocation);
                if (errorList != null && errorList.Count != 0)
                {
                    return new ObjectValidationError<Location>
                    {
                        Object = myConstructionSite.SingleLocation,
                        ErrorList = errorList
                    };
                }
            }
            return null;
        } 

        
        public List<ObjectValidationError<Employee>> DoCrudWithEmployees(MyConstructionSite myConstructionSite)
        {
            _employeesService.SetForeignKeysInEmployees(myConstructionSite);
            List<ObjectValidationError<Employee>> error =
                _employeesService.PostNewPayloadEmployees(myConstructionSite);
            if (error != null && error.Count != 0)
            {
                return error;
            }
            error = _employeesService.DeleteOldEmployeesNotIncludedInPayload(myConstructionSite);
            _employeesService.UpdateEmployees(myConstructionSite);
            return error ?? null;
        }

        
        public List<ObjectValidationError<Defect>> DoCrudWithDefectsPayload(MyConstructionSite myConstructionSite)
        {
            _defectsService.SetForeignKeysInDefects(myConstructionSite);
            List<ObjectValidationError<Defect>> operationError = 
                _defectsService.PostNewPayloadDefects(myConstructionSite);
            if (operationError != null)
            {
                return operationError;
            }
            operationError = 
                _defectsService.DeleteOldDefectsNotIncludedInPayload(myConstructionSite);
            if (operationError != null)
            {
                return operationError;
            }
            operationError = _defectsService.UpdatePayloadDefects(myConstructionSite);
            return operationError ?? null;
        }

        

        public List<ObjectValidationError<ImageInDevice>> DoCrudWithImageInDevicesPayload(
            MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<ImageInDevice>> operationError = 
                _imageInDevicesService.PostNewPayloadImageInDevices(myConstructionSite);
            if (operationError != null && operationError.Count != 0)
            {
                return operationError;
            }
            operationError = _imageInDevicesService.
                DeleteOldImageInDevices(myConstructionSite);
            return operationError ?? null;
        }

        public List<ObjectValidationError<Status>> DoCrudWithStatusesPayload(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Status>> operationError =
                _statusService.PostNewPayloadStatuses(myConstructionSite);
            if (operationError != null && operationError.Count != 0)
            {
                return operationError;
            }
            operationError = _statusService.DeleteDefectStatusesNotInPayload(
                myConstructionSite);
            return operationError ?? null;
        }

        public List<MyConstructionSite> GetMyConstructionSitesOfLoggedUser(string userId, int deviceId)
        {
            List<Employee> userEmployeeAccounts = _employeesService.GetEmployees()
                .Where(o => o.UserId == userId).ToList();
            if (userEmployeeAccounts.Count == 0)
            {
                return null;
            }
            List<MyConstructionSite> myConstructionSites = new List<MyConstructionSite>(userEmployeeAccounts.Count);
            foreach (Employee userEmployeeAccount in userEmployeeAccounts)
            {
                ConstructionSite constructionSite =
                    _constructionSitesService.GetConstructionSite(userEmployeeAccount.ConstructionSiteId);
                if (constructionSite.IsActive)
                {
                    myConstructionSites.Add(new MyConstructionSite
                    {
                        Employees =
                            _employeesService.GetAllEmployeesFromConstructionSite(userEmployeeAccount.ConstructionSiteId),
                        SingleConstructionSite =
                            _constructionSitesService.GetConstructionSite(userEmployeeAccount.ConstructionSiteId),
                        SingleLocation =
                            _locationsService.GetLocationFromConstructionSite(userEmployeeAccount.ConstructionSiteId)
                    });
                }
            }
            foreach (MyConstructionSite myConstructionSite in myConstructionSites)
            {
                List<MyDefect> myDefects = new List<MyDefect>();
                List <Defect> defectsFromConstructionSite = _defectsService.
                    GetAllDefectsFromConstructionSite(myConstructionSite.SingleConstructionSite.Id);
                foreach (Defect defect in defectsFromConstructionSite)
                {
                    myDefects.Add(new MyDefect
                    {
                        SingleDefect = defect
                    });
                }
                foreach (MyDefect myDefect in myDefects)
                {
                    List<MyStatus> myStatuses = new List<MyStatus>();
                    List<Status> statusesFromDefect = _statusService.GetAllStatusFromSameDefect(myDefect.SingleDefect.Id);
                    foreach (Status status in statusesFromDefect)
                    {
                        myStatuses.Add(new MyStatus
                        {
                            SingleStatus = status,
                            ImagesInDevice = _imageInDevicesService.
                                GetAllImageInDevicesFromStatusAndDevice(status.Id, deviceId)
                        });
                    }
                    myDefect.MyMultipleStatus = myStatuses;
                }

                myConstructionSite.MyDefects = myDefects;
            }
            return myConstructionSites;
        } 

        public int GetUploadedConstructionSiteId(ConstructionSite constructionSite)
        {
            ConstructionSite constructionSiteFromDatabase = _constructionSitesService.GetConstructionSites()
                .FirstOrDefault(
                    o => o.Compare(constructionSite));
            if (constructionSiteFromDatabase != null)
            {
                return constructionSiteFromDatabase.Id;
            }
            return -1;
        }

        

        
       

    }
}