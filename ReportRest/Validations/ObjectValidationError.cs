﻿using System.Collections.Generic;
using System.Linq;

namespace ReportRest.Validations
{
    public class ObjectValidationError <T>
    {
        public T Object { get; set; }
        public List<string> ErrorList { get; set; }

        public bool Compare(ObjectValidationError<T> other)
        {
            if (!ReferenceEquals(Object, other.Object))
            {
                return false;
            }
            return ErrorList.Except(other.ErrorList).ToArray().Length == 0;
        }
    }
}
