﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ReportRest.Documents;
using ReportRest.Models;

namespace ReportRest.Validations
{
    public class MyConstructionSiteValidator
    {

        public const string NothingSpecified = "nothing.specified";
        public const string ConstructionSiteNotSpecified = "construction.site.not.specified";
        public const string DefectsNotSpecified = "defects.not.specified";
        public const string DefectNotSpecified = "defect.not.specified";
        public const string StatusesNotSpecified = "statuses.not.specified";
        public const string StatusNotSpecified = "status.not.specified";
        public const string LocationNotSpecified = "location.not.specified";
        public const string ImageInDevicesNotSpecified = "image.in.devices.not.specified";
        public const string ImageInDeviceNotSpecified = "image.in.device.not.specified";
        public const string EmployeesNotSpecified = "employees.not.specified";
        public const string DefectForeignKeyMismatch = "defect.foreign.key.mismatch";
        public const string StatusForeignKeyMismatch = "status.foreign.key.mismatch";
        public const string ImageInDeviceForeignKeyMismatch = "image.in.device.foreign.key.mismatch";
        public const string EmployeeForeignKeyMismatch = "employee.foreign.key.mismatch";

        public static List<string> IfMandatoryUploadParametersEmptyReturnErrorList(MyConstructionSite myConstructionSite)
        {
            List<string> errorList = new List<string>();
            if (myConstructionSite == null)
            {
                errorList.Add(NothingSpecified);
            }
            if (myConstructionSite != null)
            {
                if (myConstructionSite.SingleConstructionSite == null)
                {
                    errorList.Add(ConstructionSiteNotSpecified);
                }
                if (myConstructionSite.SingleLocation == null)
                {
                    errorList.Add(LocationNotSpecified);
                }
            }
            return errorList;
        }

        public static List<ObjectValidationError<Defect>> CheckForeignKeysInDefects(
            MyConstructionSite myConstructionSite)
        {
            return (from myDefect in myConstructionSite.MyDefects
                where myDefect.SingleDefect.ConstructionSiteId != myConstructionSite.SingleConstructionSite.Id
                select new ObjectValidationError<Defect>
                {
                    Object = myDefect.SingleDefect, ErrorList = new List<string> {DefectForeignKeyMismatch}
                }).ToList();
        }

        public static List<ObjectValidationError<Status>> CheckForeignKeysInStatuses(
            MyConstructionSite myConstructionSite)
        {
            return (from myDefect in myConstructionSite.MyDefects
                from myStatus in myDefect.MyMultipleStatus
                where myStatus.SingleStatus.DefectId != myDefect.SingleDefect.Id
                select new ObjectValidationError<Status>
                {
                    Object = myStatus.SingleStatus, ErrorList = new List<string> {StatusForeignKeyMismatch}
                }).ToList();
        }

        public static List<ObjectValidationError<ImageInDevice>> CheckForeignKeysInImagesInDevice(
            MyConstructionSite myConstructionSite)
        {
            return (from myDefect in myConstructionSite.MyDefects
                from myStatus in myDefect.MyMultipleStatus
                where myStatus.ImagesInDevice != null
                from imageInDevice in myStatus.ImagesInDevice
                where imageInDevice.StatusId != myStatus.SingleStatus.Id
                select new ObjectValidationError<ImageInDevice>
                {
                    Object = imageInDevice, ErrorList = new List<string> {ImageInDeviceForeignKeyMismatch}
                }).ToList();
        }

        public static List<ObjectValidationError<Employee>> CheckForeignKeysInEmployees(
            MyConstructionSite myConstructionSite)
        {
            return (from employee in myConstructionSite.Employees
                where employee.ConstructionSiteId != myConstructionSite.SingleConstructionSite.Id
                select new ObjectValidationError<Employee>
                {
                    Object = employee, ErrorList = new List<string> {EmployeeForeignKeyMismatch}
                }).ToList();
        }   

        public static List<ObjectValidationError<MyDefect>> IfDefectFieldsAreEmptyReturnError(List<MyDefect> myDefects)
        {
            List<ObjectValidationError<MyDefect>> emptyFieldErrors = new List<ObjectValidationError<MyDefect>>();
            foreach (MyDefect myDefect in myDefects)
            {
                List<string> errorList = new List<string>();
                if (myDefect.SingleDefect == null)
                {
                    errorList.Add(DefectNotSpecified);
                }
                if (myDefect.MyMultipleStatus == null || myDefect.MyMultipleStatus.Count == 0)
                {
                    errorList.Add(StatusesNotSpecified);
                }
                if (errorList.Count != 0)
                {
                    emptyFieldErrors.Add(
                        new ObjectValidationError<MyDefect>
                        {
                            Object = myDefect,
                            ErrorList = errorList
                        });
                }
            }
            if (emptyFieldErrors.Count != 0)
            {
                return emptyFieldErrors;
            }
            return null;
        }

        public static List<ObjectValidationError<MyStatus>> IfMyStatusFieldsAreEmptyReturnError(List<MyDefect> myDefects)
        {
            List<ObjectValidationError<MyStatus>> emptyFieldErrors = new List<ObjectValidationError<MyStatus>>();
            foreach (MyDefect myDefect in myDefects)
            {
                foreach (MyStatus myStatus in myDefect.MyMultipleStatus)
                {
                    if (myStatus != null)
                    {
                        List<string> errorList = new List<string>();
                        if (myStatus.SingleStatus == null)
                        {
                            errorList.Add(StatusNotSpecified);
                        }
                        if (errorList.Count != 0)
                        {
                            emptyFieldErrors.Add(
                                new ObjectValidationError<MyStatus>
                                {
                                    Object = myStatus,
                                    ErrorList = errorList
                                });
                        }
                    }
                }
                    
            }

            if (emptyFieldErrors.Count != 0)
            {
                return emptyFieldErrors;
            }
            return null;
        }

    }
}