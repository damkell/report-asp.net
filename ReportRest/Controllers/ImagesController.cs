﻿using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using ReportRest.Validations;

namespace ReportRest.Controllers
{
    public class ImagesController : ApiController
    {

        private readonly ImagesService _imagesService = new ImagesService();
        private readonly ImageInDevicesService _imageInDevicesService = new ImageInDevicesService();
        public const string ImageInDeviceMustBeSpecified = "image.in.device.must.be.specified";
        public const string ImageMustBeSpecified = "image.must.be.specified";
        public const string NothingSpecified = "nothing.specified";
        public const string IdNotSpecified = "id.not.specified";
        public const string ListOfIdsNotSpecified = "list.of.ids.not.specified";
        public const string IdCannotBeNull = "id.cannot.be.null";

        [Route("UploadImage")]
        [HttpPost]
        public ResponseMessageResult UploadImage(MyImage image)
        {
            ResponseMessageResult mandatoryFieldsEmptyResult = IfMandatoryFieldsAreEmptyGetError(image);
            if (mandatoryFieldsEmptyResult != null)
            {
                return mandatoryFieldsEmptyResult;
            }
            ResponseMessageResult imageUploadErrorResult = UploadImageAndGetValidationErrors(image);
            if (imageUploadErrorResult != null)
            {
                return imageUploadErrorResult;
            }
            ResponseMessageResult imageInDeviceValidationErrorResult = UploadImageInDevicesAndGetValidationErrors(image);
            if (imageInDeviceValidationErrorResult != null)
            {
                return imageInDeviceValidationErrorResult;
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [HttpGet]
        [Route("GetImage")]
        public ResponseMessageResult GetImage(int id)
        {
            if (id == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, IdNotSpecified));
            }
            Image image = _imagesService.GetImage(id);
            if (image == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ImagesService.ImageNotExist));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, image));
        }

        [HttpDelete]
        [Route("DeleteImage")]
        public ResponseMessageResult DeleteImage(int id)
        {
            if (id == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, IdNotSpecified));
            }
            Image image = _imagesService.GetImage(id);
            if (image == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ImagesService.ImageNotExist));
            }
            _imagesService.DeleteImage(id);
            DeleteImagesInDeviceWithImageId(id);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [HttpPost]
        [Route("DeleteMultipleImages")]
        public ResponseMessageResult DeleteMultipleImages(List<int> idsList)
        {
            if (idsList == null || idsList.Count == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ListOfIdsNotSpecified));
            }
            if (idsList.Any(id => id == 0))
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, IdCannotBeNull));
            }
            ResponseMessageResult imageNotExistResult = IfImageNotExistReturnError(idsList);
            if (imageNotExistResult != null)
            {
                return imageNotExistResult;
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [HttpGet]
        [Route("GetMultipleImages")]
        public ResponseMessageResult GetMultipleImages(List<int> idsList)
        {
            if (idsList == null || idsList.Count == 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, ListOfIdsNotSpecified));
            }
            if (idsList.Any(id => id == 0))
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, IdCannotBeNull));
            }
            List<ObjectValidationError<int>> errorList = new List<ObjectValidationError<int>>();
            List<Image> imageList = new List<Image>();
            foreach (int id in idsList)
            {
                Image image = _imagesService.GetImage(id);
                if (image == null)
                {
                    List<string> serviceErrorList = new List<string> {ImagesService.ImageNotExist};
                    ObjectValidationError<int> error = new ObjectValidationError<int>
                    {
                        Object = id,
                        ErrorList = serviceErrorList
                    };
                    errorList.Add(error);
                }
                imageList.Add(image);
            }
            if (errorList.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, errorList));
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, imageList));
        }

        private ResponseMessageResult IfImageNotExistReturnError(List<int> idsList)
        {
            List<ObjectValidationError<int>> errorsList = (from id in idsList
                                                           let doesExist = _imagesService.DeleteImage(id)
                                                           where !doesExist
                                                           let errorList = new List<string> { ImagesService.ImageNotExist }
                                                           select new ObjectValidationError<int>
                                                           {
                                                               Object = id,
                                                               ErrorList = errorList
                                                           }).ToList();
            if (errorsList.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, errorsList));
            }
            return null;
        }

        private ResponseMessageResult UploadImageInDevicesAndGetValidationErrors(MyImage image)
        {
            Image imageWithId = _imagesService.GetImages().FirstOrDefault(o => o.Compare(image.SingleImage));

            List<ObjectValidationError<ImageInDevice>> objectValidationErrors =
                new List<ObjectValidationError<ImageInDevice>>();
            foreach (ImageInDevice imageInDevice in image.ImageInDevices)
            {
                if (imageWithId != null)
                {
                    imageInDevice.ImageId = imageWithId.Id;
                    List<string> errorList = _imageInDevicesService.PostImageInDevice(imageInDevice);
                    if (errorList != null && errorList.Count != 0)
                    {
                        _imagesService.DeleteImage(imageWithId.Id);
                        ObjectValidationError<ImageInDevice> error = new ObjectValidationError<ImageInDevice>
                        {
                            Object = imageInDevice,
                            ErrorList = errorList
                        };
                        objectValidationErrors.Add(error);
                    }
                }
            }

            if (objectValidationErrors.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, objectValidationErrors));
            }
            return null;
        }

        private ResponseMessageResult UploadImageAndGetValidationErrors(MyImage image)
        {
            if (image.ImageInDevices != null && image.ImageInDevices.Count != 0)
            {
                List<string> imageValidationErrors = _imagesService.PostImage(image.SingleImage);
                if (imageValidationErrors != null && imageValidationErrors.Count != 0)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                        new ObjectValidationError<Image>
                        {
                            Object = image.SingleImage,
                            ErrorList = imageValidationErrors
                        }
                    ));
                }
            }
            return null;
        }

        private ResponseMessageResult IfMandatoryFieldsAreEmptyGetError(MyImage myImage)
        {
            List<string> errorList = new List<string>();
            if (myImage == null)
            {
                errorList.Add(NothingSpecified);
            }
            if (myImage != null)
            {
                if (myImage.SingleImage == null)
                {
                    errorList.Add(ImageMustBeSpecified);
                }
                if (myImage.ImageInDevices == null || myImage.ImageInDevices.Count == 0)
                {
                    errorList.Add(ImageInDeviceMustBeSpecified);
                }
            }
            if (errorList.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,errorList));
            }
            return null;
        }

        private void DeleteImagesInDeviceWithImageId(int imageId)
        {
            List<ImageInDevice> imageInDeviceList = _imageInDevicesService.GetImageInDevices().Where(
                o => o.ImageId == imageId).ToList();
            if (imageInDeviceList.Count != 0)
            {
                foreach (ImageInDevice imageInDevice in imageInDeviceList)
                {
                    _imageInDevicesService.DeleteImageInDevice(imageInDevice.Id);
                }
            }
        }

    }
}
