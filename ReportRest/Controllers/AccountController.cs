﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;

namespace ReportRest.Controllers
{
    [Authorize]
    [RoutePrefix("api/Account")]
    public class AccountController : ApiController
    {
        private const string LocalLoginProvider = "Local";
        private ApplicationUserManager _userManager;
        private readonly PendingEventsService _pendingEventsService = new PendingEventsService();
        private readonly EmployeesService _employeesService = new EmployeesService();

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager,
            ISecureDataFormat<AuthenticationTicket> accessTokenFormat)
        {
            UserManager = userManager;
            AccessTokenFormat = accessTokenFormat;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ISecureDataFormat<AuthenticationTicket> AccessTokenFormat { get; private set; }

        // GET api/Account/UserInfo
        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("UserInfo")]
        public UserInfoViewModel GetUserInfo()
        {
            ExternalLoginData externalLogin = ExternalLoginData.FromIdentity(User.Identity as ClaimsIdentity);

            return new UserInfoViewModel
            {
                Email = User.Identity.GetUserName(),
                HasRegistered = externalLogin == null,
                LoginProvider = externalLogin?.LoginProvider
            };
        }

        [Route("CheckIfUserExists")]
        [HttpGet]
        [AllowAnonymous]
        public bool CheckIfUserExists(string key)
        {
            return UserManager.FindByEmail(key) != null || UserManager.FindByName(key) != null; 
        }

        [Route("SendExampleEmail")]
        [HttpPost]
        [AllowAnonymous]
        public IHttpActionResult SendExampleEmail(string emailAddress)
        {
            try
            {
                EmailService.SendEmail("mantttttas@gmail.com", "Tgx*ctgx=1");
                return Ok();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
        }

        // POST api/Account/Logout
        [Route("Logout")]
        public IHttpActionResult Logout()
        {
            Authentication.SignOut(CookieAuthenticationDefaults.AuthenticationType);
            return Ok();
        }

        // GET api/Account/ManageInfo?returnUrl=%2F&generateState=true
        [Route("ManageInfo")]
        public async Task<ManageInfoViewModel> GetManageInfo(string returnUrl, bool generateState = false)
        {
            IdentityUser user = await UserManager.FindByIdAsync(User.Identity.GetUserId());

            if (user == null)
            {
                return null;
            }

            List<UserLoginInfoViewModel> logins = user.Logins.Select(linkedAccount => new UserLoginInfoViewModel
            {
                LoginProvider = linkedAccount.LoginProvider, ProviderKey = linkedAccount.ProviderKey
            }).ToList();

            if (user.PasswordHash != null)
            {
                logins.Add(new UserLoginInfoViewModel
                {
                    LoginProvider = LocalLoginProvider,
                    ProviderKey = user.UserName,
                });
            }

            return new ManageInfoViewModel
            {
                LocalLoginProvider = LocalLoginProvider,
                Email = user.UserName,
                Logins = logins,
            };
        }

        // POST api/Account/ChangePassword
        [Route("ChangePassword")]
        public async Task<IHttpActionResult> ChangePassword(ChangePasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword,
                model.NewPassword);
            
            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/SetPassword
        [Route("SetPassword")]
        public async Task<IHttpActionResult> SetPassword(SetPasswordBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            return Ok();
        }

        // POST api/Account/Register
        [AllowAnonymous]
        [Route("Register")]
        public async Task<IHttpActionResult> Register(RegisterBindingModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = new ApplicationUser() { UserName = model.UserName, Email = model.Email };

            PendingEvent maybeEvent = _pendingEventsService.GetPendingEvents().
                Single(o => o.Hash == model.InvitationHash);
            if (maybeEvent != null && maybeEvent.IsActive
                && maybeEvent.EventType == PendingEvent.Event.Invitation)
            {

                user.EmailConfirmed = true;
                IdentityResult firstResult = await UserManager.CreateAsync(user, model.Password);

                if (!firstResult.Succeeded)
                {
                    return GetErrorResult(firstResult);
                }

                ApplicationUser currentUser = UserManager.FindByEmailAsync(user.Email).Result;

                List<string> errorList = _employeesService.PostEmployee(new Employee
                {
                    ConstructionSiteId = maybeEvent.ConstructionSiteId,
                    IsManager = maybeEvent.IsManager,
                    UserId = currentUser.Id,
                    IsActive = true,
                    CreatedAt = DateTime.Now,
                    CreatorId = maybeEvent.InviterId
                });

                if (errorList == null)
                {
                    _pendingEventsService.DisablePendingEvent(maybeEvent);
                    return Ok();
                }
            }

            IdentityResult result = await UserManager.CreateAsync(user, model.Password);

            if (!result.Succeeded)
            {
                return GetErrorResult(result);
            }

            _pendingEventsService.CreateRegistrationEvent(model.Email);

            return Ok();
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("ConfirmEmail")]
        public ResponseMessageResult ConfirmEmail(string hash)
        {
            PendingEvent pendingEventWithHash = _pendingEventsService.GetPendingEvents().
                Single(o => o.Hash == hash);
            if (pendingEventWithHash == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                    PendingEventsService.PendingEventNotExist));
            }
            if (pendingEventWithHash.IsActive && 
                pendingEventWithHash.EventType == PendingEvent.Event.Registration)
            {
                ApplicationUser user = UserManager.Users.FirstOrDefault(
                o => o.Email == pendingEventWithHash.Email);
                if (user == null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.InternalServerError));
                }
                user.EmailConfirmed = true;
                UserManager.Update(user);
                _pendingEventsService.DisablePendingEvent(pendingEventWithHash);
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                UserManager.Dispose();
            }

            base.Dispose(disposing);
        }

        #region Helpers

        private IAuthenticationManager Authentication => Request.GetOwinContext().Authentication;

        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }

        private class ExternalLoginData
        {
            public string LoginProvider { get; private set; }

            public static ExternalLoginData FromIdentity(ClaimsIdentity identity)
            {
                Claim providerKeyClaim = identity?.FindFirst(ClaimTypes.NameIdentifier);

                if (string.IsNullOrEmpty(providerKeyClaim?.Issuer) || string.IsNullOrEmpty(providerKeyClaim.Value))
                {
                    return null;
                }

                if (providerKeyClaim.Issuer == ClaimsIdentity.DefaultIssuer)
                {
                    return null;
                }

                return new ExternalLoginData
                {
                    LoginProvider = providerKeyClaim.Issuer,
                };
            }
        }
        #endregion
    }
}
