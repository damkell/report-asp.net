﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;

namespace ReportRest.Controllers
{
    [System.Web.Http.RoutePrefix("Employees")]
    public class EmployeesController : ApiController
    {

        private readonly ConstructionSitesService _constructionSitesService =
            new ConstructionSitesService();
        private readonly EmployeesService _employeesService = new EmployeesService();
        private readonly PendingEventsService _pendingEventsService = 
            new PendingEventsService();

        private ApplicationUserManager _manager;

        [RequireHttps]
        [System.Web.Http.Authorize]
        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddEmployee")]
        public ResponseMessageResult AddEmployee(EmployeeForm employeeForm)
        {
            if (employeeForm.ValidationErrors() != null)
            {
                return
                    ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, employeeForm.ValidationErrors()));
            }
            ConstructionSite constructionSite = _constructionSitesService.GetConstructionSite(employeeForm.ConstructionSiteId);
            if (constructionSite == null)
            {
                return
                    ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                        ConstructionSitesService.SiteNotExist));
            }
            string currentUserId = HttpContext.Current.User.Identity.GetUserId();
            if (!string.IsNullOrEmpty(currentUserId))
            {
                List<Employee> constructionSiteEmployees =
                    _employeesService.GetAllEmployeesFromConstructionSite(employeeForm.ConstructionSiteId);
                Employee inviter = constructionSiteEmployees
                    .FirstOrDefault(o => o.UserId == currentUserId);
                if (inviter != null && !inviter.IsManager)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                        EmployeesService.NotAManager));
                }
                _manager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
                ApplicationUser user = _manager.FindByEmailAsync(employeeForm.Email).Result;
                if (user == null)
                {
                    List<string> errorList = _pendingEventsService.CreateInvitationEvent(employeeForm.Email,
                        constructionSite.Id, currentUserId, employeeForm.IsManager);
                    if (errorList != null)
                    {
                        return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, errorList));
                    }
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
                }
                Employee toInvite = constructionSiteEmployees
                    .FirstOrDefault(o => o.UserId == user.Id);
                if (toInvite != null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                        EmployeesService.EmployeeAlreadyExists));
                }
                List<string> errorList2 = _pendingEventsService.CreateAdditionEvent(currentUserId,
                    constructionSite.Id, employeeForm.IsManager, user.Id);
                if (errorList2 != null)
                {
                    return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, errorList2));
                }
            }

            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("JoinTeam")]
        public ResponseMessageResult JoinTeam(int pendingEventId)
        {
            PendingEvent pendingEvent = _pendingEventsService.
                GetPendingEvent(pendingEventId);
            if (pendingEvent == null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,
                    PendingEventsService.PendingEventNotExist));
            }
            Employee employee = new Employee
            {
                IsActive = true,
                IsManager = pendingEvent.IsManager,
                ConstructionSiteId = pendingEvent.ConstructionSiteId,
                CreatedAt = DateTime.Now,
                CreatorId = pendingEvent.InviterId,
                UserId = pendingEvent.UserId
            };
            List<string> postErrors = _employeesService.PostEmployee(employee);
            if (postErrors != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, postErrors));
            }
            _pendingEventsService.DisablePendingEvent(pendingEvent);
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }
    }
}
