﻿using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using ReportRest.Models;
using ReportRest.Services;

namespace ReportRest.Controllers
{

    [RequireHttps]
    [System.Web.Http.Authorize]
    [System.Web.Http.RoutePrefix("Main")]
    public class MainController : ApiController
    {

        private ApplicationUserManager userManager;
        private readonly PendingEventsService _pendingEventsService = new PendingEventsService();
        private readonly MyConstructionSiteService _myConstructionSiteService = new MyConstructionSiteService();

        [System.Web.Http.HttpGet]
        [System.Web.Http.Route("MyData")]
        public ResponseMessageResult GetAllLoggedUserData(int deviceId)
        {
            userManager = Request.GetOwinContext().GetUserManager<ApplicationUserManager>();
            string currentUserId = HttpContext.Current.User.Identity.GetUserId();
            ApplicationUser currentUser = userManager.FindByIdAsync(currentUserId).Result;
            MyUser myUser = new MyUser
            {
                Email = currentUser.Email,
                UserName = currentUser.UserName,
                EmailConfirmed = currentUser.EmailConfirmed,
                PendingEvents = _pendingEventsService.GetPendingEventsOfUser(currentUserId),
                MyConstructionSites = _myConstructionSiteService.GetMyConstructionSitesOfLoggedUser(currentUserId, deviceId)
            };
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK, myUser));
        }
    }
}
