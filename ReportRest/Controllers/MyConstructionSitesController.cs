﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Security;
using ReportRest.Documents;
using ReportRest.Models;
using ReportRest.Services;
using ReportRest.Validations;

namespace ReportRest.Controllers
{
    [RequireHttps]
    [System.Web.Http.Authorize]
    public class MyConstructionSitesController : ApiController
    {

        private readonly MyConstructionSiteService _service = new MyConstructionSiteService();

        [System.Web.Http.HttpPost]
        [System.Web.Http.Route("AddConstructionSite")]
        public ResponseMessageResult AddConstructionSite(MyConstructionSite myConstructionSite)
        {
            List<string> emptyFieldsError = 
                MyConstructionSiteValidator.IfMandatoryUploadParametersEmptyReturnErrorList(myConstructionSite);
            if (emptyFieldsError != null && emptyFieldsError.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, emptyFieldsError));
            }
            ResponseMessageResult uploadError = UploadDataAndIfItIsNotValidGetError(myConstructionSite);
            if (uploadError != null)
            {
                return uploadError;
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        [System.Web.Http.HttpPut]
        [System.Web.Http.Route("UpdateConstructionSite")]
        public ResponseMessageResult UpdateConstructionSite(MyConstructionSite myConstructionSite)
        {
            ResponseMessageResult mandatoryFieldsEmptyError = IfMandatoryUpdateFieldsAreEmptyGetError(myConstructionSite);
            if (mandatoryFieldsEmptyError != null)
            {
                return mandatoryFieldsEmptyError;
            }
            ResponseMessageResult locationModificationError = AnalizeUpdateLocation(myConstructionSite);
            if (locationModificationError != null)
            {
                return locationModificationError;
            }
            ResponseMessageResult defectOperationsError = AnalizeCrudDefects(myConstructionSite);
            if (defectOperationsError != null)
            {
                return defectOperationsError;
            }
            ResponseMessageResult statusOperationsError = AnalizeCrudStatuses(myConstructionSite);
            if (statusOperationsError != null)
            {
                return statusOperationsError;
            }
            ResponseMessageResult imageInDevicesOperationsError =
                AnalizeCrudImageInDevices(myConstructionSite);
            if (imageInDevicesOperationsError != null)
            {
                return imageInDevicesOperationsError;
            }
            return ResponseMessage(Request.CreateResponse(HttpStatusCode.OK));
        }

        private ResponseMessageResult AnalizeUpdateLocation(MyConstructionSite myConstructionSite)
        {
            ObjectValidationError<Location> error = _service.UpdateLocation(myConstructionSite);
            if (error != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, error));
            }
            return null;
        }

        private ResponseMessageResult AnalizeCrudStatuses(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Status>> error = 
                _service.DoCrudWithStatusesPayload(myConstructionSite);
            if (error != null && error.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, error));
            }
            return null;
        }

        private ResponseMessageResult AnalizeCrudImageInDevices(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<ImageInDevice>> error =
                _service.DoCrudWithImageInDevicesPayload(myConstructionSite);
            if (error != null && error.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, error));
            }
            return null;
        }

        private ResponseMessageResult AnalizeCrudDefects(MyConstructionSite myConstructionSite)
        {
            List<ObjectValidationError<Defect>> error =
                MyConstructionSiteValidator.CheckForeignKeysInDefects(myConstructionSite);
            if (error != null && error.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, error));
            }
            error = _service.DoCrudWithDefectsPayload(myConstructionSite);
            if (error != null && error.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,error));
            }
            return null;
        }

        private ResponseMessageResult IfMandatoryUpdateFieldsAreEmptyGetError(MyConstructionSite myConstructionSite)
        {
            List<string> emptyLocationAndSiteErrors = 
                MyConstructionSiteValidator.IfMandatoryUploadParametersEmptyReturnErrorList(myConstructionSite);
            if (myConstructionSite.MyDefects == null || myConstructionSite.MyDefects.Count == 0)
            {
                emptyLocationAndSiteErrors.Add(MyConstructionSiteValidator.DefectsNotSpecified);
            }
            if (myConstructionSite.Employees == null || myConstructionSite.Employees.Count == 0)
            {
                emptyLocationAndSiteErrors.Add(MyConstructionSiteValidator.EmployeesNotSpecified);
            }
            if (emptyLocationAndSiteErrors.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,emptyLocationAndSiteErrors));
            }
            List<ObjectValidationError<MyDefect>> emptyMyDefectListFields =
                MyConstructionSiteValidator.IfDefectFieldsAreEmptyReturnError(myConstructionSite.MyDefects);
            if (emptyMyDefectListFields != null && emptyMyDefectListFields.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest, emptyMyDefectListFields));
            }
            List<ObjectValidationError<MyStatus>> emptyMyStatusListFields =
                MyConstructionSiteValidator.IfMyStatusFieldsAreEmptyReturnError(myConstructionSite.MyDefects);
            if (emptyMyStatusListFields != null && emptyMyStatusListFields.Count != 0)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,emptyMyStatusListFields));
            }
            return null;
        }

        

        public ResponseMessageResult UploadDataAndIfItIsNotValidGetError(MyConstructionSite myConstructionSite)
        {
            ObjectValidationError<ConstructionSite> constructionSiteUploadError =
                _service.PostConstructionSiteAndIfItIsNotValidGetError(myConstructionSite.SingleConstructionSite);
            if (constructionSiteUploadError != null)
            {
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,constructionSiteUploadError));
            }
            myConstructionSite.SingleLocation.ConstructionSiteId =
                _service.GetUploadedConstructionSiteId(myConstructionSite.SingleConstructionSite);
            ObjectValidationError<Location> locationUploadError =
                _service.PostLocationAndIfItIsNotValidGetError(myConstructionSite.SingleLocation);
            if (locationUploadError != null)
            {
                _service.DeleteConstructionSiteFromModelWithoudId(myConstructionSite.SingleConstructionSite);
                return ResponseMessage(Request.CreateResponse(HttpStatusCode.BadRequest,locationUploadError));
            }
            _service.PostCreatorAsFirstConstructionSiteEmployee(myConstructionSite);
            return null;
        }
        
    }
}