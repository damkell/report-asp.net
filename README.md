# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This product allows construction companies to register defects and to make the repairs faster. This product is reliable because it has about 80% coverage of integration tests and remaining 20% coverage of manual tests.
* Version 1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Apps that must be installed to get this app running: ###
* Microsoft .Net Framework 4.6.1
* IIS 10.0 Express
* Microsoft SQL Server Compact 4.0

### Steps to get this app running ###
* Install Visual Studio 2015
* Open Visual Studio
* Create new empty Web project
* Copy downloaded source from this repository to new project
* Go to email service class, and change SenderAddress to your company email address, and SenderPassword to your company email password.
* Compile and run

### To run tests: ###
* Install Visual Studio 2015
* Open Visual Studio
* Create new empty Web project
* Copy downloaded source from this repository to new project
* Open NuGet Package Manager
* Install NUnit Visual Studio Adapter
* Install NUnit Core
* Click Test -> Run All Tests

### REST documentation ###
* First of all, user needs to register. [Registration](http://s10.postimg.org/v4kmojwzd/Register.png) All fields are mandatory. After the registration the confirmation email will be sent.
* Then you need to obtain OAUTH token. [Obtain Token](http://s12.postimg.org/9b9emueul/Token.png) Notice that headers must be set to like in [this](http://s17.postimg.org/fhzzm7p6n/Header.png)
* On each request pass token as [this](http://s27.postimg.org/lj6vast83/Bearer.png)
* Get all current user information like [this](http://s24.postimg.org/4e592d01h/Current_User_Information.png)
* Add new construction site like [this](http://s23.postimg.org/x9ax1wdjf/Add_New_Construction_Site.png)
* If you will attempt to add exactly same construction site, you will get this [error](http://s13.postimg.org/yvvi46dmv/Add_New_Construction_Site_Error.png)
* After you add new ConstructionSite, user data should look [like](http://s21.postimg.org/qi2lqp953/User_Data_After_New_Construction_Site.png)
* Update ConstructionSite in calling [this address](http://postimg.org/image/cbe2tvjkx/6d3996c4/) and use [this payload](http://pastebin.com/6pk8kZeM). ImagesInDevice are not mandatory!


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact